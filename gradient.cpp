#include "gradient.h"
#include "util.h"

void documentGradientApprox(SparseMatrix& data,
						int numberOfTokens,
						int numberOfDocuments,
						LowRankMatrix& currentSolution,
						LowRankMatrix& oldSolution,
						SparseMatrix& gradient,
						int* wordCounts,
						int doc,
						int docSize,
						double* buffer,
						double alpha,
						int softmaxSample,
						int* randomSet,
						int* sample,
						double minValueThreshold,
						bool accelerated,
						double t,
						double nt,
						double& anorm,
						double& loglikelihood,
						double& total
						)
{
	int uniqueTokens = data.documentSizes[doc+1]-data.documentSizes[doc];
	if(uniqueTokens+softmaxSample>numberOfTokens)
		softmaxSample = numberOfTokens-uniqueTokens;
	//Sample a set of unique words of size 'softaxSample'
	randomSubset(randomSet,sample,
		numberOfTokens,softmaxSample,
		uniqueTokens,&data.tokenIds[data.documentSizes[doc]]);

	//recover statistics for sample and tokens from low rank solution in buffer and bufferPos
	double* bufferPos = new double[uniqueTokens];
	double nmv = 5000; //useless
	double lsNeg; // = sampledLogSum(currentSolution,doc,buffer,nmv,softmaxSample,sample);
	double lsPos; //= sampledLogSum(currentSolution,doc,bufferPos,nmv,docSize,&data.tokenIds[data.documentSizes[doc]]);
	if(accelerated)
	{
		lsNeg = sampledLogSumAccelerated(currentSolution,oldSolution,1.0+(t-1)/nt,-(t-1)/nt,doc,buffer,nmv,softmaxSample,sample);	
		lsPos = sampledLogSumAccelerated(currentSolution,oldSolution,1.0+(t-1)/nt,-(t-1)/nt,doc,bufferPos,nmv,uniqueTokens,&data.tokenIds[data.documentSizes[doc]]);
	}
	else
	{
		lsNeg = sampledLogSum(currentSolution,doc,buffer,nmv,softmaxSample,sample);
		lsPos = sampledLogSum(currentSolution,doc,bufferPos,nmv,uniqueTokens,&data.tokenIds[data.documentSizes[doc]]);
	}

	int upsample = ((numberOfTokens-uniqueTokens)/softmaxSample);

	double Z = log(exp(lsNeg)*upsample + exp(lsPos));
	Z = lsPos + log(exp(lsNeg-lsPos) + 1); 
	// #warning "hack for upsample = 1"	
	double pLeftOver = (1 - (exp(lsNeg - Z) + exp(lsPos -Z)));
	double zero = 0;
	if(numberOfTokens-uniqueTokens-softmaxSample>0)
		zero = pLeftOver/(numberOfTokens-uniqueTokens-softmaxSample);
	gradient.zero[doc] = - alpha * docSize * zero;				
	//std::cout << lsNeg << " " << lsPos << " " << uniqueTokens << " " << softmaxSample << " " << Z << " " << gradient.zero[doc] << std::endl;
	//compute loglikelihood and gradient for the tokens of the document

	int i=0;
	for(int j=data.documentSizes[doc];j<data.documentSizes[doc+1];j++)
	{
		wordCounts[data.tokenIds[j]]++;
		const double p = exp(bufferPos[i]-Z);
		double g = -data.tokenFrequencies[j] + p * docSize;
		gradient.tokenIds.push_back(data.tokenIds[j]);
		gradient.tokenFrequencies.push_back(-alpha * g);
		gradient.documentIds.push_back(0);
		gradient.documentFrequencies.push_back(0.0);
		
		if(-log2(p)>100)
		{
			loglikelihood+=100*data.tokenFrequencies[j];
		}
		else
		{
			loglikelihood+=-log2(p)*data.tokenFrequencies[j];
		}
			
		total+=data.tokenFrequencies[j];
		i++;
	}
	for(int j=0;j<softmaxSample;j++)
	{
		const double p = exp(buffer[j]-Z);
		//round to default value for more sparsity
		if(fabs(p-zero)<minValueThreshold)
			continue;			
		int token = sample[j];
		wordCounts[token]++;
		double g = p * docSize;
		gradient.tokenIds.push_back(token);
		gradient.tokenFrequencies.push_back(-alpha * g);
		gradient.documentIds.push_back(0);
		gradient.documentFrequencies.push_back(0.0);
	}
	delete[] bufferPos;
	gradient.documentSizes.push_back(gradient.tokenIds.size());
}

void documentGradient(SparseMatrix& data,
						int numberOfTokens,
						int numberOfDocuments,
						LowRankMatrix& currentSolution,
						LowRankMatrix& oldSolution,
						SparseMatrix& gradient,
						int* wordCounts,
						int doc,
						int docSize,
						double* buffer,
						double alpha,
						bool accelerated,
						double t,
						double nt,
						double& anorm,
						double& loglikelihood,
						double& total,
						double& minValueThreshold
						)
{
	double nmv=100000;
	double ls;
	if(accelerated)
		ls = logSumAccelerated(currentSolution,oldSolution,1.0+(t-1)/nt,-(t-1)/nt,doc,buffer,nmv);
	else
		ls = logSum(currentSolution,doc,buffer,nmv);

	int j = data.documentSizes[doc];
	int zeroCounter = 0;
	double zeroAvg = 0.0;
	for(int tok=0;tok<numberOfTokens;tok++)
	{
		double p = exp(buffer[tok]-ls);
		double g = p * docSize;
		if(j< data.documentSizes[doc+1] && tok==data.tokenIds[j])
		{
			g+=-data.tokenFrequencies[j];
			if(-log2(p)>100)
			{
				//std::cout << p << "(" << bufferPos[i] << "-" << Z << ") ";
				loglikelihood+=100*data.tokenFrequencies[j];
			}
			else
			{
				loglikelihood+=-log2(p)*data.tokenFrequencies[j];
			}
			total+=data.tokenFrequencies[j];
			j++;
		}
		anorm+=(buffer[tok]+g)*(buffer[tok]+g);

		if(g>0 && g<docSize * minValueThreshold)
		{
			zeroCounter++;
			zeroAvg+=g;
		//	continue;
		}

		wordCounts[tok]++;
		gradient.tokenIds.push_back(tok);
		gradient.tokenFrequencies.push_back(-alpha * g);
		gradient.documentIds.push_back(0);
		gradient.documentFrequencies.push_back(0.0);
	}
	if(zeroCounter>0)
		gradient.zero[doc] = -alpha * zeroAvg / zeroCounter;
	gradient.documentSizes.push_back(gradient.tokenIds.size());
}

void documentGradientHierarchical(SparseMatrix& data,
						int numberOfTokens,
						int numberOfDocuments,
						LowRankMatrix& currentSolution,
						LowRankMatrix& oldSolution,
						SparseMatrix& gradient,
						int* wordCounts,
						int doc,
						int docSize,
						double* buffer,
						double alpha,
						bool accelerated,
						double t,
						double nt,
						double& anorm,
						double& loglikelihood,
						double& total,
						Hierarchy& h,
						int* helper
						)
{
	//std::cout << (t-1)/nt << std::endl;
	int zeroCounter = 0;
	double zeroAvg = 0.0;
	for(int j = data.documentSizes[doc]; j< data.documentSizes[doc+1]; j++)
	{
		int target = data.tokenIds[j];
		total+=data.tokenFrequencies[j];
		const std::vector<bool>& binaryCode = h.codes[target];
		const std::vector<int32_t>& pathToRoot = h.paths[target];
		// std::cout << target << " ";
		// for (int32_t i = 0; i < pathToRoot.size(); i++) 
		// 	std::cout << binaryCode[i];
		// std::cout << std::endl;
		double l=0;
		for (int32_t i = 0; i < pathToRoot.size(); i++) {
			double x;
			if(!accelerated)
				x = lowRankEntry(currentSolution,pathToRoot[i],doc);
			else
				x = (1.0+(t-1)/nt) * lowRankEntry(currentSolution,pathToRoot[i],doc)
				  - (t-1)/nt * lowRankEntry(oldSolution,pathToRoot[i],doc); //TODO
			double sigmoid = 1.0 / (1 + exp(-x));
			//std::cout << sigmoid << " ";
			int sign = -1;
			if(!binaryCode[i])
				sign = 1;
			if(binaryCode[i])
			{	
				if(-log2(sigmoid)>100)
					l+=100;
				else
					l +=-log2(sigmoid);
			}
			else
			{
				if(-log2(1-sigmoid)>100)
					l+=100;
				else
					l+=-log2(1-sigmoid);
			}
			double g = data.tokenFrequencies[target] * (sigmoid - (binaryCode[i] ? 1 : 0));
			anorm+=alpha * alpha * g*g;
			if(helper[pathToRoot[i]]==0)
			{
				wordCounts[pathToRoot[i]]++;
				helper[pathToRoot[i]]=gradient.tokenIds.size();
				gradient.tokenIds.push_back(pathToRoot[i]);
				gradient.tokenFrequencies.push_back(-alpha * g);
				gradient.documentIds.push_back(0);
				gradient.documentFrequencies.push_back(0.0);

			}
			else
			{
				gradient.tokenFrequencies[helper[pathToRoot[i]]]+=-alpha * g;
			}
		}
		// std::cout << std::endl;
		loglikelihood+=data.tokenFrequencies[target]*l;
	}
	for(int i=gradient.documentSizes[gradient.documentSizes.size()-1];i<gradient.tokenIds.size();i++)
		helper[gradient.tokenIds[i]] = 0;
	

	//WE SHOULD PROBABLY CLEAN UP THE GRADIENT AND MERGE DUPLICATE ENTRIES
	gradient.zero[doc] = 0;
	gradient.documentSizes.push_back(gradient.tokenIds.size());
}