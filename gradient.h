#pragma once
#include "matrix.h"
#include "hierarchy.h"

void documentGradientApprox(SparseMatrix& data,
						int numberOfTokens,
						int numberOfDocuments,
						LowRankMatrix& currentSolution,
						LowRankMatrix& oldSolution,
						SparseMatrix& gradient,
						int* wordCounts,
						int doc,
						int docSize,
						double* buffer,
						double alpha,
						int softmaxSample,
						int* randomSet,
						int* sample,
						double minValueThreshold,
						bool accelerated,
						double t,
						double nt,
						double& anorm,
						double& loglikelihood,
						double& total
						);

void documentGradient(SparseMatrix& data,
						int numberOfTokens,
						int numberOfDocuments,
						LowRankMatrix& currentSolution,
						LowRankMatrix& oldSolution,
						SparseMatrix& gradient,
						int* wordCounts,
						int doc,
						int docSize,
						double* buffer,
						double alpha,
						bool accelerated,
						double t,
						double nt,
						double& anorm,
						double& loglikelihood,
						double& total,
						double& minValueThreshold
						);


void documentGradientHierarchical(SparseMatrix& data,
						int numberOfTokens,
						int numberOfDocuments,
						LowRankMatrix& currentSolution,
						LowRankMatrix& oldSolution,
						SparseMatrix& gradient,
						int* wordCounts,
						int doc,
						int docSize,
						double* buffer,
						double alpha,
						bool accelerated,
						double t,
						double nt,
						double& anorm,
						double& loglikelihood,
						double& total,
						Hierarchy& h,
						int* helper
						);