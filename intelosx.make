#
#
# Copyright (c) 2016-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.
#

OS := $(shell uname)
# ifeq ($(OS),Darwin)
#   	# Run MacOS commands
#   	CXX = g++
# 	CXXFLAGS = -g3 -O3 -pthread -std=c++11 -Wall -fopenmp
# 	LINKERFLAGS = -lgfortran -mkl
# else
	# check for Linux and run other commands
CXX = icpc
# CXXFLAGS = -g3 -O0 -pthread -std=c++11 -Wall 
CXXFLAGS = -g3 -O3 -pthread -std=c++11 -Wall -qopenmp -DUSE_INTEL_MKL
#GFORTRANPATH = /usr/local/gfortran/lib/
LINKERFLAGS = -mkl 
# LINKERFLAGS = -lgfortran -lblas -L${GFORTRANPATH}
#CBLASPATH=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/Headers/
# -fopenmp
# endif

# -Wc++11-extensions
OBJS = args.o dictionary.o matrix.o vector.o utils.o 
INCLUDES = -I.


all: CXXFLAGS += -mtune=native -march=native
all: svd test pythonwrapper 
	${CXX} ${LINKERFLAGS} svd.o main.o kernelmachine.o kernel.o matrix.o util.o -o kernelmachine

debug: CXXFLAGS += -g -O0 -fno-inline
debug: all

.PHONY: fasttext propack test

test: 
	${CXX} ${CXXFLAGS} ${INCLUDES} -c main.cpp matrix.cpp kernel.cpp util.cpp kernelmachine.cpp

svd: 
	${CXX} ${CXXFLAGS} ${INCLUDES}  -c svd.cpp
propack: 
	cd propack && $(MAKE) lib
pythonwrapper:
	cd python-wrapper && $(MAKE)
clean:
	rm -rf *.o

clean-all: clean
	cd propack && $(MAKE) clean

valgrind: debug
	valgrind --leak-check=full ./doc2vex supervised -input supersimple.txt -lambda 0 -epoch 1000  -minCount 0 -softmaxSample 0 -initialRank 11  
	#valgrind --leak-check=full ./doc2vex supervised -input amazonReviews.txt -minCount 5 
