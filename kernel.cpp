#include "kernel.h"
#include <cmath>

#ifdef USE_INTEL_MKL
#include <mkl.h>
#else
#include <cblas.h>
#endif

#include <iostream>
/**
 ** CAUTION: X and Y are ROW-major, while everything else I've written is COL-Major due to Fortran
 **/
void invertedPolynomialKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d,void* hyper)
{
	#pragma omp parallel for
	for(long i=0;i<k.m;i++)
	{
		for(long j=0;j<k.n;j++)
		{
			double dot   = cblas_ddot(d,&X[iX[i]*d], 1,&Y[iY[j]*d],1)+1; //X[iX[i]*d+k] i,k => i*d+k
			double normX = cblas_dnrm2(d,&X[iX[i]*d],1);
			normX = std::sqrt(1.0+normX*normX);
			double normY = cblas_dnrm2(d,&Y[iY[j]*d],1);
			normY = std::sqrt(1.0+normY*normY);
			k.X[j*k.m+i] = 1.0/(2.0-dot/(normX*normY));
			for(int l=1;l<2;l++)
				k.X[j*k.m+i] = 1.0/(2.0-k.X[j*k.m+i]);
		}
	}	
}

void arcCosineKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper)
{
	const double pi = 3.14159265;
	// static double arccosine[256] ={0.0};
	// static bool initialized = 0;
	
	// if(!initialized)
	// {
	// 	for(int i=0;i<256;i++)
	// 	{
	// 		double t = -1.0+(i/256.0)*2.0;
	// 		arccosine[i] = acos(t);
	// 		arccosine[i] = 1.0/pi * (sin(arccosine[i]) + (pi - arccosine[i]) * cos(arccosine[i]));
	// 	}
	// 	initialized=1;
	// }
	#pragma omp parallel for
	for(long i=0;i<k.m;i++)
	{
		for(long j=0;j<k.n;j++)
		{
			double dot   = cblas_ddot(d,&X[iX[i]*d], 1,&Y[iY[j]*d],1); //X[iX[i]*d+k] i,k => i*d+k
			double normX = cblas_dnrm2(d,&X[iX[i]*d],1)+0.00001;
			normX = std::sqrt(1+normX*normX);
			double normY = cblas_dnrm2(d,&Y[iY[j]*d],1)+0.00001;
			normY = std::sqrt(1+normY*normY);
			// double t = dot/(normX*normY);
			double t = (1+dot)/(normX * normY);
			if(t<-1)
				t=-1;
			if(t>1)
				t=1;
			
			// double arc = arccosine[(int)(((1+t)*128-0.5))];
			// k.X[j*k.m+i] = normX * normY * arc;
			t = acos(t);
			k.X[j*k.m+i] = normX * normY * 1.0/pi * (sin(t) + (pi - t) * cos(t));
			for(int kk=1;kk<1;kk++)
			{
				t = (k.X[j*k.m+i]+1)/(std::sqrt(normX*normX+1) * std::sqrt(normY*normY+1));
				if(t<-1)
				 	t=-1;
				if(t>1)
				 	t=1;
				t = acos(t);
				k.X[j*k.m+i] = normX * normY * 1.0/pi * (sin(t) + (pi - t) * cos(t));
			}

		}
	}	
}
void rbfKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper)
{
	#pragma omp parallel for
	for(long i=0;i<k.m;i++)
	{
		for(long j=0;j<k.n;j++)
		{
			double dot   = cblas_ddot(d,&X[iX[i]*d],1,&Y[iY[j]*d],1); //X[iX[i]*d+k] i,k => i*d+k
			double normX = cblas_ddot(d,&X[iX[i]*d],1,&X[iX[i]*d],1);
			double normY = cblas_ddot(d,&Y[iY[j]*d],1,&Y[iY[j]*d],1);
			k.X[j*k.m+i] = normX - 2*dot + normY;
			// for(int l=0;l<5;l++)
			k.X[j*k.m+i] = exp(-1 * k.X[j*k.m+i]);
		}
	}	
}
// void chiSquaredKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper)
// {
// 	#pragma omp parallel for
// 	for(int i=0;i<k.m;i++)
// 	{
// 		for(int j=0;j<k.n;j++)
// 		{
// 			k.X[j*k.m+i] = 0;
// 			for(int l=0;l<d;l++)
// 			{
// 				if((X[iX[i]*d+l]+Y[iY[j]*d+l])<0.00000001)
// 					k.X[j*k.m+i] += 0;
// 				else	
// 					k.X[j*k.m+i] += 2*(X[iX[i]*d+l]*Y[iY[j]*d+l])/(X[iX[i]*d+l]+Y[iY[j]*d+l]);
// 			}
// 			k.X[j*k.m+i] /= 1.0*d;
// 		}
// 	}
// }

void chiSquaredKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper)
// void kernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d)
{
	const double PI = 3.14159265359;
	int kernel_width=4;
	int kernel_height=4;
	int stride=2;
	int image_width=32;
	int image_height=32;
	int depth=3;
	int* width = new int[depth+1];
	int* height = new int[depth+1];
	width[0]=image_width;
	height[0]=image_height;
	for(int i=1;i<=depth;i++)
	{
		width[i] =  (width[i-1]-kernel_width)/stride+1;
		height[i] = (height[i-1]-kernel_height)/stride+1;
	}
	#pragma omp parallel for
	for(int i=0;i<k.m;i++)
	{
		double* buffer = new double[width[1]*height[1]];
		for(int j=0;j<k.n;j++)
		{
			//std::cout << "First Layer ";
			//First Convolution on input data
			for(int y=0;y<height[1];y++)
			{
				for(int x=0;x<width[1];x++)
				{
					double dot = 0.0;
					double norm1 = 0.0001;
					double norm2 = 0.0001;
					for(int kx=0;kx<kernel_width;kx++)
					{
						for(int ky=0;ky<kernel_height;ky++)
						{
							int feature = x*stride+kx+(y*stride+ky)*width[0];
							dot  +=X[iX[i]*d+feature]*Y[iY[j]*d+feature];
							norm1+=X[iX[i]*d+feature]*X[iX[i]*d+feature];
							norm2+=Y[iY[j]*d+feature]*Y[iY[j]*d+feature];
						}
					}
					double theta=dot/std::sqrt(norm1*norm2);
					if(theta<-1)
						theta = -1;
					if(theta>1)
						theta = 1;
					buffer[x+y*width[1]]=(std::sqrt(1-theta*theta)+(PI-std::acos(theta))*theta)/PI;
					//std::cout << dot<<"("<<buffer[x+y*width[1]] << ") ";
				}
			}
			//std::cout << std::endl;
			for(int layer=1;layer<depth;layer++)
			{
				for(int x=0;x<width[layer+1];x++)
				{
					for(int y=0;y<height[layer+1];y++)
					{
						double theta = 0.0;
						for(int kx=0;kx<kernel_width;kx++)
						{
							for(int ky=0;ky<kernel_height;ky++)
							{
								int feature = x*stride+kx+(y*stride+ky)*width[layer];
								theta+=buffer[feature];
							}
						}
						theta/=(kernel_width*kernel_height);
						buffer[x+y*width[layer+1]]=(std::sqrt(1-theta*theta)+(PI-std::acos(theta))*theta)/PI;
					}
				}
			}
			double theta = 0.0;
			for(int a=0;a<width[depth]*height[depth];a++)
				theta+=buffer[a];
			theta/=(width[depth]*height[depth]);
			k.X[j*k.m+i] = (std::sqrt(1-theta*theta)+(PI-std::acos(theta))*theta)/PI;
		}
		delete[] buffer;
	}	
}

#include <random>
#include <omp.h>
#include <vector>

inline double cleanup(double retvalue, double* minV, double* maxV, int* nonuniformsample, int nn, int d)
{
	for(int i=nn-1;i>=0;i--)
	{
		minV[nonuniformsample[i]]=0.0;
		maxV[nonuniformsample[i]]=1.0;
	}
	for(int i=nn-1;i>=0;i--)
	{
		// std::cout << nonuniformsample[i] <<"<>" << nonuniformsample[nonuniformsample[i]]  << " ";
		//switch nonuniformsample[i] and i
		nonuniformsample[nonuniformsample[i]]=nonuniformsample[i];
		nonuniformsample[i]=i;
	}
	return retvalue;
}

double sampleTree(double* x, double* y, int d, double* minV, double* maxV, int* nonuniformsample, double lambda, std::uniform_real_distribution<double>& runif,std::mt19937& gen)
{
	
	double freeMass = d;
	int nn = 0;
	for(int i=0;i<50;i++)
	{
		double lambdaPrime = lambda + std::log(1.0-runif(gen)) / freeMass;
		if(lambdaPrime < 0)
			return cleanup(1.0,minV,maxV,nonuniformsample,nn,d);
		//select split feature
		double r = runif(gen)*freeMass;
		int split;
		double cumsum = 0.0;
		for(split=0;split<nn;split++)
		{
			if(cumsum+(maxV[nonuniformsample[split]]-minV[nonuniformsample[split]])>r)
				break;
			cumsum+=(maxV[nonuniformsample[split]]-minV[nonuniformsample[split]]);
		}
		if(split==d) //every feature is nonuniform and we sampled the last one
			split=nonuniformsample[d-1];
		else if(split==nn) //split in the uniform features
		{
			// sample uniformly from the remaining features
			double ratio = (r-cumsum)/(freeMass-cumsum);
			int bla = nn+((int)((d-nn) * ratio ));
			if(bla>d)
				bla=d-1;
			split = nonuniformsample[bla];
			//move spit to the nonuniform array
			nonuniformsample[bla] = nn;
			nonuniformsample[nn] = bla;
			nn++;
		}
		else //Split in the first, nonuniform featuers
			split=nonuniformsample[split];
		double splitX = minV[split] + (maxV[split]-minV[split])*runif(gen);
		if(x[split]<splitX && y[split]>=splitX)
			return cleanup(0.0,minV,maxV,nonuniformsample,nn,d);
		if(x[split]>=splitX && y[split]<splitX)
			return cleanup(0.0,minV,maxV,nonuniformsample,nn,d);
		freeMass-=maxV[split]-minV[split];
		if(x[split]<splitX)
			maxV[split]=splitX;
		else
			minV[split]=splitX;
		freeMass+=maxV[split]-minV[split];
		lambda = lambdaPrime;
	}
	return cleanup(0.0,minV,maxV,nonuniformsample,nn,d);
}

void mondrianForestKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper)
{
	// std::random_device rd[omp_get_num_threads()];  //Will be used to obtain a seed for the random number engine
    std::vector<std::mt19937> gen; //Standard mersenne_twister_engine seeded with rd()
    std::vector<std::uniform_real_distribution<double>> runif;//[(0.0, 1.0);
    //std::vector<std::exponential_distribution<double>> rexp;//(1);
    // std::cout << omp_get_num_threads() << " " << omp_get_max_threads() << std::endl;
    for(int i=0;i<omp_get_max_threads();i++)
    {
    	gen.push_back(std::mt19937(i));
    	runif.push_back(std::uniform_real_distribution<double>(0.0,1.0));
    //	rexp.push_back(std::exponential_distribution<double>(1.0));
    }
    double* minV = new double[d*omp_get_max_threads()];
    double* maxV = new double[d*omp_get_max_threads()];
    int* helperBuffer = new int[d*omp_get_max_threads()];
    for(int i=0;i<omp_get_max_threads();i++)
    {
    	for(int j=0;j<d;j++)
    	{
    		minV[i*d+j] = 0.0;
    		maxV[i*d+j] = 1.0;
    		helperBuffer[i*d+j] = j;
    	}
    }
    #pragma omp parallel for
	for(long i=0;i<k.m;i++)
	{
		int t = omp_get_thread_num();
		double lambda = std::pow(1024,1.0/d)-1.0;
		//std::cout << "t"<<t << std::endl;
		for(long j=0;j<k.n;j++)
		{
			double distance = 0;
			for(long l=0;l<d;l++)
			{
				distance+=std::abs(X[iX[i]*d+l]-Y[iY[j]*d+l]);
			}
			k.X[j*k.m+i] = std::exp(-lambda * distance);
			// k.X[j*k.m+i]=0;
			// const int numTrees = 1000;
			// for(int tree = 0;tree<numTrees;tree++)
			// {
			// 	double r = sampleTree(&X[iX[i]*d],&Y[iY[j]*d], d, &minV[t*d], &maxV[t*d], &helperBuffer[t*d], std::pow(4096,1.0/d)-1.0,runif[t],gen[t]);
			// 	k.X[j*k.m+i]+=1.0/numTrees * r;
			// 	// if(r>0)
			// 	// 	std::cout << "x";
			// 	// else
			// 	// 	std::cout << "-";
			// }

			// // std::cout << std::endl;
		}
	}
	// std::cout << std::endl;
	delete[] minV;
	delete[] maxV;
	delete[] helperBuffer;
}
