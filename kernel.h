#pragma once
#include "matrix.h"


/**
 * computes the kernel matrix between X[iX] and Y[iY], where 
 * - k.m is the length of iX
 * - k.n is the length of iY
 * - d is the number of features
 * 		used in the computation (uses X[iX+0],...,X[iX+d-1])
 */
void invertedPolynomialKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper);
void arcCosineKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper);
void rbfKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper);
void chiSquaredKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper);
void mondrianForestKernel(DenseMatrix& k, double* X, double* Y, int* iX, int* iY, int d, void* hyper);