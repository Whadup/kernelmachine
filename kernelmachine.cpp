#include "kernelmachine.h"
#include "matrix.h"
#include "util.h"
#include "kernel.h"
#include "svd.h"
#include <cstring>
#include <iostream>
#ifdef USE_INTEL_MKL
#include <mkl.h>
#else
#include <cblas.h>
#endif
#include <chrono>

KernelMachine::~KernelMachine()
{
	if(this->centers)
		delete[] this->centers;
	if(this->alpha)
		delete[] this->alpha;
	if(this->D.U)
	{
		delete[] this->D.U;
		delete[] this->D.S;
	}
	if(this->Khat.S)
		delete[] this->Khat.S;
	if(this->nystroemMap)
		delete[] this->nystroemMap;

}

double KernelMachine::hilbertNorm()
{
        double tmp=0.0;
        for(int l=0;l<this->numberOfLabels;l++)
        {
                for(int i=0;i<Khat.k;i++)
                        tmp+=alpha[i+Khat.k*l]*alpha[i+Khat.k*l]/Khat.S[i];
        }
        return std::sqrt(tmp);

}


void nystroem(LowRankMatrix& Khat,LowRankMatrix& D,DenseMatrix& kernelM,DenseMatrix& kernelMM,int N, int& K,double* nystroemMap)
{
	std::chrono::high_resolution_clock::time_point timeMeasurement = std::chrono::high_resolution_clock::now();
	int sampleSize = kernelMM.m;
	LowRankMatrix factorization;
	int lowRank = sampleSize;
	// lowRank = 1200;
	makeSymmetricLowRankMatrix(factorization,sampleSize,sampleSize);
	lowRank = eig(sampleSize,factorization.k,kernelMM.X,factorization.U,factorization.S,sampleSize);
	if(lowRank<sampleSize)
		std::cout << "WARNING: RANK DEFICIENT KERNEL" << std::endl;
	
	if(K>lowRank)
		K=lowRank;

	std::cout << "Done with SVD "<<factorization.S[0]<< " " << factorization.S[K-1] << " " << lowRank << std::endl;
	makeSymmetricLowRankMatrix(Khat,N,lowRank);
	D.S = new double[lowRank];
	D.m = N;
	D.k = lowRank;

	// C(N x m)
	// W(m x m) = U∑U^t
	// U(m x k)
	// G(N x k) = CU
	//Khat.U = kernelMM.X * factorization.U
	std::cout << "Factorization " << kernelM.m << "x" << Khat.k << " rank " <<  Khat.k << std::endl;
	cblas_dgemm(CblasColMajor,   
			CblasNoTrans,
			CblasNoTrans,
			Khat.m,              //M = N
			Khat.k,              //N = lowrank      
			sampleSize,          //K = sampleSize  
			1.0,                 //alpha
			kernelM.X,           //A   
			kernelM.m,           //LDA   
			factorization.U,     //B   
			factorization.m,     //LDB   
			0.0,                 //beta 
			Khat.U,              //C       
			Khat.m               //LDC
		);
	
	delete[] kernelM.X;
	kernelM.X = new double[1]; //evil hack

	for(long k=0;k<Khat.k;k++)
	{
		Khat.S[k] = factorization.S[k]*N/sampleSize;
		cblas_dscal(N,sqrt(1.0*sampleSize/N)/factorization.S[k],&Khat.U[((long)k)*((long)Khat.m)],1);
	}
	std:: cout << "Nyström computed " << std::chrono::duration_cast<std::chrono::seconds>( std::chrono::high_resolution_clock::now() - timeMeasurement ).count() << std::endl;;
	timeMeasurement = std::chrono::high_resolution_clock::now();
	std::cout << "allocating second factorization " << std::endl;
	double* QR = new double[((long)N) * ((long)Khat.k)];
	double* tau = new double[Khat.k];
	std::cout << "QR Decomposition" << std::endl;
	//Copy the factor Khat.U for QR decomposition
	memcpy(QR,Khat.U,((long)N)*((long)Khat.k)*sizeof(Khat.U[0]));
	//Compute QR decomposition of Khat.U and store it in QR,tau
	mkl_verbose(1);
	int info = LAPACKE_dgeqrf(LAPACK_COL_MAJOR, N, Khat.k, QR,N,tau);
	mkl_verbose(0);
	double* R = new double[Khat.k*Khat.k];
	for(long i=0;i<Khat.k;i++)
	{	for(long j=0;j<Khat.k;j++)
		{
			if(j<=i)
				R[i*Khat.k+j]=QR[i*N+j];
			else
				R[i*Khat.k+j]=0.0;
		}
	}
	//Compute RS^(1/2) and store it in the upper triangle QR
	for(long i=0;i<Khat.k;i++)
		cblas_dscal(1+i,std::sqrt(Khat.S[i]),&QR[i*N],1);
	mkl_verbose(1);
	info = LAPACKE_dlauum(LAPACK_COL_MAJOR , 'U' , Khat.k , QR , N);
	double* U = new double[((long)Khat.k)*((long)Khat.k)];
	int newK = eig(Khat.k,Khat.k,QR,U,Khat.S,N);
	if(newK < Khat.k)
		std::cout << "WARNING: LOST EIGENVALUES IN R∑R^T"<< std::endl;
	mkl_verbose(1);
	//Get the Q out of the QR factorization, store in QR
	LAPACKE_dorgqr(LAPACK_COL_MAJOR, N, Khat.k, Khat.k, QR, N, tau);
	mkl_verbose(0);
	//Multiply Q * U with Q:N x sampleSize and U:sampleSize x K
	cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,
		N,newK,Khat.k,1.0,
		QR,N,U,Khat.k,
		0.0,Khat.U,N);
	double* buffer = new double[((long)Khat.k)*Khat.k];
	//R^T * Uschlange
	cblas_dgemm(CblasColMajor,CblasTrans,CblasNoTrans,
		Khat.k,newK,Khat.k,1.0,
		R,Khat.k,U,Khat.k,
		0.0,buffer,Khat.k);
	//V * buffer
	cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,
		sampleSize,newK,Khat.k,1.0,
		factorization.U,factorization.m,buffer,Khat.k,
		0.0,nystroemMap,sampleSize);
	delete[] buffer;
	for(int i=0;i<Khat.k;i++)
		cblas_dscal(sampleSize,1.0/Khat.S[i],&nystroemMap[i*sampleSize],1);
	mkl_verbose(0);
	std::cout << "Nyström Map "<< nystroemMap[0] << " " << nystroemMap[Khat.k*Khat.k-1] << std::endl;
	std::cout << "Preconditioner computed " << std::chrono::duration_cast<std::chrono::seconds>( std::chrono::high_resolution_clock::now() - timeMeasurement ).count() << std::endl;
	D.U = Khat.U;
	for(long i=0;i<Khat.k;i++)
		D.S[i] = Khat.S[i];
	Khat.k = newK;
	D.k = newK;
	delete[] R;
	delete[] U;
	delete[] QR;
	delete[] tau;
	delete[] factorization.U;
	delete[] factorization.S;
}

void nystroemApproximate(LowRankMatrix& Khat,LowRankMatrix& D,DenseMatrix& kernelM,DenseMatrix& kernelMM,int N, int& K,double*& nystroemMap)
{
	std::chrono::high_resolution_clock::time_point timeMeasurement = std::chrono::high_resolution_clock::now();
	int sampleSize = kernelMM.m;
	LowRankMatrix factorization;
	int lowRank = sampleSize;
	// lowRank = 1200;
	makeSymmetricLowRankMatrix(factorization,sampleSize,sampleSize);
	lowRank = eig(sampleSize,factorization.k,kernelMM.X,factorization.U,factorization.S,sampleSize);
	if(lowRank<sampleSize)
		std::cout << "WARNING: RANK DEFICIENT KERNEL" << std::endl;
	
	if(K>lowRank)
		K=lowRank;

	std::cout << "Done with SVD "<<factorization.S[0]<< " " << factorization.S[K-1] << " " << lowRank << std::endl;
	makeSymmetricLowRankMatrix(Khat,N,lowRank);
	D.S = new double[lowRank];
	D.m = N;
	D.k = lowRank;

	// C(N x m)
	// W(m x m) = U∑U^t
	// U(m x k)
	// G(N x k) = CU
	//Khat.U = kernelMM.X * factorization.U
	std::cout << "Factorization " << kernelM.m << "x" << Khat.k << " rank " <<  Khat.k << std::endl;
	cblas_dgemm(CblasColMajor,   
			CblasNoTrans,
			CblasNoTrans,
			Khat.m,              //M = N
			Khat.k,              //N = lowrank      
			sampleSize,          //K = sampleSize  
			1.0,                 //alpha
			kernelM.X,           //A   
			kernelM.m,           //LDA   
			factorization.U,     //B   
			factorization.m,     //LDB   
			0.0,                 //beta 
			Khat.U,              //C       
			Khat.m               //LDC
		);
	
	delete[] kernelM.X;
	kernelM.X = new double[1]; //evil hack

	for(long k=0;k<Khat.k;k++)
	{
		Khat.S[k] = factorization.S[k]*N/sampleSize;
		cblas_dscal(N,sqrt(1.0*sampleSize/N)/factorization.S[k],&Khat.U[((long)k)*((long)Khat.m)],1);
	}
	std:: cout << "Nyström computed " << std::chrono::duration_cast<std::chrono::seconds>( std::chrono::high_resolution_clock::now() - timeMeasurement ).count() << std::endl;;
	// timeMeasurement = std::chrono::high_resolution_clock::now();
	// std::cout << "allocating second factorization " << std::endl;
	// double* QR = new double[((long)N) * ((long)Khat.k)];
	// double* tau = new double[Khat.k];
	// std::cout << "QR Decomposition" << std::endl;
	// //Copy the factor Khat.U for QR decomposition
	// memcpy(QR,Khat.U,((long)N)*((long)Khat.k)*sizeof(Khat.U[0]));
	// //Compute QR decomposition of Khat.U and store it in QR,tau
	// mkl_verbose(1);
	// int info = LAPACKE_dgeqrf(LAPACK_COL_MAJOR, N, Khat.k, QR,N,tau);
	// mkl_verbose(0);
	// double* R = new double[Khat.k*Khat.k];
	// for(long i=0;i<Khat.k;i++)
	// {	for(long j=0;j<Khat.k;j++)
	// 	{
	// 		if(j<=i)
	// 			R[i*Khat.k+j]=QR[i*N+j];
	// 		else
	// 			R[i*Khat.k+j]=0.0;
	// 	}
	// }
	// //Compute RS^(1/2) and store it in the upper triangle QR
	// for(long i=0;i<Khat.k;i++)
	// 	cblas_dscal(1+i,std::sqrt(Khat.S[i]),&QR[i*N],1);
	// mkl_verbose(1);
	// info = LAPACKE_dlauum(LAPACK_COL_MAJOR , 'U' , Khat.k , QR , N);
	// double* U = new double[((long)Khat.k)*((long)Khat.k)];
	// int newK = eig(Khat.k,Khat.k,QR,U,Khat.S,N);
	// if(newK < Khat.k)
	// 	std::cout << "WARNING: LOST EIGENVALUES IN R∑R^T"<< std::endl;
	// mkl_verbose(1);
	// //Get the Q out of the QR factorization, store in QR
	// LAPACKE_dorgqr(LAPACK_COL_MAJOR, N, Khat.k, Khat.k, QR, N, tau);
	// mkl_verbose(0);
	// //Multiply Q * U with Q:N x sampleSize and U:sampleSize x K
	// cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,
	// 	N,newK,Khat.k,1.0,
	// 	QR,N,U,Khat.k,
	// 	0.0,Khat.U,N);
	nystroemMap = factorization.U;
	for(int i=0;i<Khat.k;i++)
		cblas_dscal(sampleSize,sqrt(1.0*sampleSize/N)/Khat.S[i],&nystroemMap[i*sampleSize],1);
	// mkl_verbose(0);
	// std::cout << "Nyström Map "<< nystroemMap[0] << " " << nystroemMap[Khat.k*Khat.k-1] << std::endl;
	// std::cout << "Preconditioner computed " << std::chrono::duration_cast<std::chrono::seconds>( std::chrono::high_resolution_clock::now() - timeMeasurement ).count() << std::endl;
	D.U = Khat.U;
	for(long i=0;i<Khat.k;i++)
		D.S[i] = Khat.S[i];
	D.k = Khat.k;
	// delete[] R;
	// delete[] U;
	// delete[] QR;
	// delete[] tau;
	// delete[] factorization.U;
	delete[] factorization.S;
}

void computeLowRankApproximation(LowRankMatrix& G,int N,int K,DenseMatrix& kernelMM,DenseMatrix& kernelM)
{
	int sampleSize = kernelMM.m;
	LowRankMatrix factorization;
	//makeLowRankMatrix(factorization,sampleSize,sampleSize,K);
	// svd(sampleSize,sampleSize,factorization.k,&denseMatrixVector,
	//		 factorization.U,factorization.V,factorization.S,10000,(void*)&kernelMM);
	makeSymmetricLowRankMatrix(factorization,sampleSize,K);
	eig(sampleSize,factorization.k,kernelMM.X,factorization.U,factorization.S,sampleSize);
	std::cout << "Done with SVD "<<factorization.S[0]<< " " << factorization.S[K-1] << std::endl;
	
	
	std::cout << "Compute Large Factorization" << std::endl;
	
	
	// C(N x m)
	// W(m x m) = U∑U^t
	// U(m x k)
	// G(N x k) = CU
	for(int k=0;k<K;k++)
		G.S[k] = factorization.S[k] * (1.0*N/sampleSize);
	//Approximate Eigenvectors and Gram Schmidt
	for(int j=0;j<K;j++)
	{
		//Compute C*U
		for(int i=0;i<N;i++)
		{
			G.U[j*G.m+i] = cblas_ddot(sampleSize,&kernelM.X[i],kernelM.m,&factorization.U[j*factorization.m],1);
		}
		//Gram Schmidt
		//std::cout << "dot ";
		for(int j2=0;j2<j;j2++)
		{
			//DOT
			double dot = cblas_ddot(N,&G.U[j*G.m],1,&G.U[j2*G.m],1);
		//	std::cout << dot << " ";
			for(int i=0;i<N;i++)
				G.U[j*G.m+i]-=dot*G.U[j2*G.m+i];
		}
		//std::cout << std::endl;
		double norm = cblas_dnrm2(N,&G.U[j*G.m],1);
		//std::cout << "norm "<<j<<" " << norm << std::endl;
		cblas_dscal(N,1.0/norm,&G.U[j*G.m],1);
	}
	// for(int i=0;i<sampleSize;i++)
	// {
	// 	for(int j=0;j<K;j++)
	// 	{
	// 		std::cout << factorization.U[j*N+i] << "\t";
	// 	}
	// 	std::cout << std::endl;
	// }
}

void rmseLoss(double* gradient,int* Y,int* minibatch,int i,int numberOfLabels,int batchsize,int N,double& loss)
{
	for(int l=0;l<numberOfLabels;l++)
	{			
		// LOSS
		double y = -1.0;
		if(Y[minibatch[i]]==l)
			y = 1.0;
		loss+= (y - gradient[l*batchsize+i]) * (y-gradient[l*batchsize+i])/N;
		gradient[l*batchsize+i] = 1.0/(batchsize) * (gradient[l*batchsize+i] - y);
		// std::cout << gradient[l*batchsize+i] << " ";
	}
	// std::cout << std::endl;
}

void hingeLoss(double* gradient,int* Y,int* minibatch,int i,int numberOfLabels,int batchsize,int N,double& loss)
{
	for(int l=0;l<numberOfLabels;l++)
	{			
		// LOSS
		double y = 0.0;
		if(Y[minibatch[i]]==l)
			y = 1.0;
		else
			y = -1.0;
		loss += (1-y*gradient[l*batchsize+i]) < 0 ? 0 : (1.0-y*gradient[l*batchsize+i])/N;
		// loss+= (y - gradient[l*batchsize+i]) * (y-gradient[l*batchsize+i])/N;
		// gradient[l*batchsize+i] = 1.0/(batchsize) * (gradient[l*batchsize+i] - y);
		if(1-gradient[l*batchsize+i]*y>0)
			gradient[l*batchsize+i] = 1.0/(batchsize) * - y;
		else
			gradient[l*batchsize+i] = 0.0;
	}
}

void squaredHingeLoss(double* gradient,int* Y,int* minibatch,int i,int numberOfLabels,int batchsize,int N,double& loss)
{
	for(int l=0;l<numberOfLabels;l++)
	{			
		// LOSS
		double y = 0.0;
		if(Y[minibatch[i]]==l)
			y = 1.0;
		else
			y = -1.0;
		loss += (1-y*gradient[l*batchsize+i]) < 0 ? 0 : (1.0-y*gradient[l*batchsize+i])*(1.0-y*gradient[l*batchsize+i])/N;
		// loss+= (y - gradient[l*batchsize+i]) * (y-gradient[l*batchsize+i])/N;
		// gradient[l*batchsize+i] = 1.0/(batchsize) * (gradient[l*batchsize+i] - y);
		if(1-gradient[l*batchsize+i]*y>0)
			gradient[l*batchsize+i] = -y * (1.0-y*gradient[l*batchsize+i])/batchsize;
		else
			gradient[l*batchsize+i] = 0.0;
		if(Y[minibatch[i]]==1)
			gradient[l*batchsize+i]*=2;
	}
}

void epoche(int& N,int& K, int& d,int& batchsize, int& numberOfLabels,DenseMatrix& kernelM,double* X, int* Y,
	int* indices,int* minibatch,double* alpha,double* gradient,double *fullGradBuffer,double* lowRankBuffer,
	LowRankMatrix& D, double& eta, 
	void(*kernel)(DenseMatrix&,double*,double*, int*, int*, int, void*),void* hyper,
	LowRankMatrix& Khat, double* nystroemBuffer,bool nystroemSampling,bool conditioning,
	void(*lossFunction)(double*,int*,int*,int,int,int,int,double&))
{
	double loss = 0;
	int accuracy = 0;
	for(int firstExample=0;firstExample<N;firstExample+=batchsize)
	{
		int broken = 0;
		if(firstExample+batchsize>N)
		{
			broken = firstExample-N+batchsize;
			firstExample=N-batchsize;
		}
		for(int j=0;j<batchsize;j++)
			minibatch[j]=firstExample+j;

		//Lots of Parallelization Potential
		if(!nystroemSampling)
		{
			kernel(kernelM,X,X,minibatch,indices,d,hyper);
			//Compute g
			cblas_dgemm(CblasColMajor,
				CblasNoTrans,
				CblasNoTrans,
				batchsize,
				numberOfLabels,
				N,
				1.0,
				kernelM.X,
				batchsize,
				alpha,
				N,
				0.0,
				gradient,
				batchsize
			);
		}
		else
		{
			#pragma omp parallel for
			for(int i=0;i<batchsize;i++)
			{
				int j = minibatch[i];
				cblas_dgemm(CblasColMajor,
					CblasNoTrans,
					CblasNoTrans,
					1,
					numberOfLabels,
					Khat.k,
					1.0,
					&Khat.U[j],
					Khat.m,
					alpha,
					Khat.k,
					0.0,
					&gradient[i],
					batchsize);
			}
		}
		for(int i=0;i<batchsize;i++)
		{
			//Accuracy
			int largestClass = 0;
			double largestClassStatistic = -1000000.0;

			for(int l=0;l<numberOfLabels;l++)
			{			
				// LOSS AND ACCURACY
				if(gradient[l*batchsize+i]>largestClassStatistic)
				{
					largestClass = l;
					largestClassStatistic = gradient[l*batchsize+i];
				}
			}

			if(i>=broken && largestClass==Y[minibatch[i]])
				accuracy++;

			lossFunction(gradient,Y,minibatch,i,numberOfLabels,batchsize,N,loss);
			
		}

		if(!conditioning)
		{
			if(!nystroemSampling)
			{
				for(int l=0;l<numberOfLabels;l++)
				{
					// std::cout << "GM part ";
					for(int i=0;i<batchsize;i++)
					{
						alpha[l*N+minibatch[i]] -= eta * gradient[l*batchsize+i]; 
					}
				}
			}
			else
			{
				for(int i=0;i<batchsize;i++)
				{
					int j = minibatch[i];	
					cblas_dgemm(CblasColMajor,
						CblasTrans,
						CblasNoTrans,
						Khat.k,          
						numberOfLabels,
						1,
						1.0,
						&Khat.U[j],
						Khat.m,
						&gradient[i],
						batchsize,
						(i==0 ? 0.0 : 1.0),
						nystroemBuffer,
						Khat.k);
				}
				for(int i=0;i<Khat.k;i++)
					cblas_dscal(numberOfLabels,Khat.S[i],&nystroemBuffer[i],Khat.k);
				cblas_daxpy(Khat.k*numberOfLabels,-eta,nystroemBuffer,1,alpha,1);
			}
		}
		else if(conditioning)
		{
			
			//K_m^t * g
			// K_m : batchsize x N
			// g: batchsize x numberOfLabels
			// Result: N x numberOf Labels
			if(!nystroemSampling)
			{
				for(int l=0;l<numberOfLabels;l++)
				{
					// std::cout << "GM part ";
					for(int i=0;i<batchsize;i++)
					{
						alpha[l*N+minibatch[i]] -= eta * gradient[l*batchsize+i]; 
					}
				}
				cblas_dgemm(CblasColMajor,
					CblasTrans,
					CblasNoTrans,
					N,                   //M
					numberOfLabels,      //N       
					batchsize,           //K   
					1.0,                 //alpha
					kernelM.X,           //A   
					batchsize,           //LDA   
					gradient,            //B   
					batchsize,           //LDB   
					0.0,                 //beta
					fullGradBuffer,      //C       
					N                    //LDC
				);
				cblas_dgemm(CblasColMajor,
					CblasTrans,
					CblasNoTrans,
					K,                  //M
					numberOfLabels,     //N
					N,                  //K
					1.0,                //alpha
					D.U,                //A
					N,                  //LDA
					fullGradBuffer,     //B
					N,                  //LDB
					0.0,                //beta
					lowRankBuffer,      //C
					K                   //LDC
				);

				for(int i=0;i<K-1;i++)
					cblas_dscal(numberOfLabels,D.S[i],&lowRankBuffer[i],K);
				cblas_dscal(numberOfLabels,0.0,&lowRankBuffer[K-1],K);			
				// D.U * fullGradBuffer
				// D.U: N x K
				// fullGradBuffer: K x numberOfLabels
				// Result: N x numberOfLabels
				cblas_dgemm(CblasColMajor,
					CblasNoTrans,
					CblasNoTrans,
					N,                  //M
					numberOfLabels,     //N
					K,                  //K
					1.0,                //alpha
					D.U,                //A
					N,                  //LDA
					lowRankBuffer,      //B
					K,     				//LDB
					0.0,                //beta
					fullGradBuffer,     //C
					N                   //LDC
				);
				//Gradient Update
				cblas_daxpy(N*numberOfLabels,eta,fullGradBuffer,1,alpha,1);
			}
			else
			{
				for(int i=0;i<batchsize;i++)
				{
					int j = minibatch[i];	
					cblas_dgemm(CblasColMajor,
						CblasTrans,
						CblasNoTrans,
						Khat.k,          
						numberOfLabels,
						1,
						1.0,
						&Khat.U[j],
						Khat.m,
						&gradient[i],
						batchsize,
						(i==0 ? 0.0 : 1.0),
						nystroemBuffer,
						Khat.k);
				}
				for(int i=0;i<Khat.k;i++)
				{
					double scale = Khat.S[i];
					if(i<K-1)
						scale-=D.S[i]*Khat.S[i]*Khat.S[i];
					cblas_dscal(numberOfLabels,scale,&nystroemBuffer[i],Khat.k);
				}
				cblas_daxpy(Khat.k*numberOfLabels,-eta,nystroemBuffer,1,alpha,1);
			}			
		}
		std::cout << "." << std::flush;
	}
	std::cout << " Accuracy: " << 1.0*accuracy/N << " Loss: " << loss << std::endl;
}

void KernelMachine::fit(double* X, int* Y, int N, int d, double* valX, int* valY, int valN,
	int numberOfEpoches, int K, int sampleSize, int kernelId, int lossId, bool nystroemSampling, bool conditioning, bool exactConditioning)
{
	if(kernelId==0)
	{
		this->kernel = &mondrianForestKernel;
		std::cout << "Using RBF-Kernel"<<std::endl;
	}
	else if(kernelId==1)
	{
		this->kernel = &chiSquaredKernel;
		std::cout << "Using ChiSquared-Kernel"<<std::endl;
	}
	else if(kernelId==2)
	{
		this->kernel = &invertedPolynomialKernel;
		std::cout << "Using InvertedPolynomial-Kernel"<<std::endl;
	}
	else if(kernelId==3)
	{
		this->kernel = &arcCosineKernel;
		std::cout << "Using ArcCosine-Kernel"<<std::endl;
	}
	// bool nystroemSampling = true;
	void(*loss)(double*,int*,int*,int,int,int,int,double&);
	// lossId = 2;
	if(lossId==0)
	{
		loss = &rmseLoss;
		std::cout << "Using RMSE Loss" << std::endl;
	}
	else if(lossId==1)
	{
		loss = &hingeLoss;
		std::cout << "Using Hinge Loss" << std::endl;
	}
	else if(lossId==2)
	{
		loss = &squaredHingeLoss;
		std::cout << "Using Squared Hinge Loss" << std::endl;
	}
	std::cout << (nystroemSampling ? "Using " : "Not using ") << "Nyström Sampling" << std::endl;
	std::cout << (conditioning ? "Using " : "Not using ") << "Conditioned Stochastic Gradient Descent" << std::endl;

	std::chrono::high_resolution_clock::time_point timeMeasurement = std::chrono::high_resolution_clock::now();

	this->N = N;
	this->d = d;
	this->sampleSize = sampleSize;
	this->nystroemSampling = nystroemSampling;
	
	int validateEvery = 5;

	int* indices = new int[N];
	for(int i=0;i<N;i++)
	{
		indices[i]=i;
	}
	int numberOfLabels = 0;
	for(int i=0;i<N;i++)
	{	if(Y[i]>=numberOfLabels)
		{
			numberOfLabels=Y[i]+1;
		}
	}
	this->numberOfLabels = numberOfLabels;
	
	int* validationBuffer;
	if(valN>0)
		validationBuffer = new int[valN];
	
	std::cout << "Prediction Problem with "
		<< N << " Examples, "
		<< d << " Attributes and "
		<< numberOfLabels << " Classes" <<std::endl;
	
	if(N<sampleSize)
		sampleSize = N;
	
	int* sample = new int[sampleSize];
	randomSubset(indices,sample,N,sampleSize);
	if(nystroemSampling)
	{
		this->alpha = new double[sampleSize*numberOfLabels];
		for(long l=0;l<sampleSize*numberOfLabels;l++)
			this->alpha[l]=0.0;
		this->centers = new double[sampleSize*d];
		for(int i=0;i<sampleSize;i++)
			memcpy(&centers[i*d],&X[sample[i]*d],d*sizeof(X[0]));
	}
	else
	{
		this->centers = new double[N*d];
		memcpy(this->centers,X,sizeof(this->centers[0])*N*d);
	}

	std::cout << "Compute kernel matrix sample of size " << sampleSize << std::endl;
	
	DenseMatrix kernelMM;
	DenseMatrix kernelM;
	makeDenseMatrix(kernelMM,sampleSize,sampleSize);
	makeDenseMatrix(kernelM,N,sampleSize);
	//kernel(kernelMM,X,X,sample,sample,d,this->hyper);
	kernel(kernelM,X,X,indices,sample,d,this->hyper);
	for(int i=0;i<sampleSize;i++)
		for(int j=0;j<sampleSize;j++)
			kernelMM.X[j*kernelMM.m+i]=kernelM.X[j*kernelM.m+sample[i]];

	std::cout << "Kernel sample computed" << std::chrono::duration_cast<std::chrono::seconds>( std::chrono::high_resolution_clock::now() - timeMeasurement ).count() << std::endl;
	timeMeasurement = std::chrono::high_resolution_clock::now();
	
	std::cout << "Compute SVD" << std::endl;
	double eta;
	if(!nystroemSampling)
	{
		this->alpha = new double[N*numberOfLabels];
		for(long l=0;l<N*numberOfLabels;l++)
			this->alpha[l]=0.0;
		makeSymmetricLowRankMatrix(this->Khat,N,K);
		computeLowRankApproximation(this->Khat,N,K,kernelMM,kernelM);
		deleteDenseMatrix(kernelMM);
		this->D.U = this->Khat.U;
		this->D.V = this->Khat.V;
		this->D.S = new double[K];
		this->D.m = this->Khat.m;
		this->D.k = K;
		for(int i=0;i<K-1;i++)
		{
			// D.S[i]= 1.0/D.S[i] * (1-ro)*D.S[K-1]/D.S[i];
			this->D.S[i] = (1  - std::sqrt(this->Khat.S[K-1] / this->Khat.S[i])) / this->Khat.S[i];
			// this->D.S[i] = (1 - 1.0*std::sqrt(this->Khat.S[i]));
			// this->D.S[i] = (1 - std::sqrt(ro * this->Khat.S[K-1] / this->Khat.S[i])) / this->Khat.S[i];
			std::cout << this->Khat.S[i] << "/" << this->D.S[i] << " ";
		}
		D.S[K-1]=0.0;
		if(conditioning)
			eta = std::sqrt(this->Khat.S[0]/this->Khat.S[K-1]) * 1.5/(2.0*this->Khat.S[0]/N);
		else
			eta = 1.5/(2.0*this->Khat.S[0]/N);
		std::cout << std::endl;
	}
	else
	{
		this->nystroemMap = new double[((long)sampleSize)*((long)sampleSize)];
		// nystroem(this->Khat,this->D,kernelM,kernelMM,N,K,this->nystroemMap);
		if(!exactConditioning)
			nystroemApproximate(this->Khat,this->D,kernelM,kernelMM,N,K,this->nystroemMap);
		else
		{
			K = sampleSize;
			nystroem(this->Khat,this->D,kernelM,kernelMM,N,K,this->nystroemMap);
		}
		if(K>this->D.k)
			K = this->D.k;
		std::cout << "USING K="<<K << std::endl;
		if(conditioning)
			eta = std::sqrt(this->D.S[0]/this->D.S[K-1]) * 1.5/(2.0*this->D.S[0]/N);
		else
			eta = 1.5/(2.0*this->D.S[0]/N);
		for(int i=0;i<K-1;i++)
		{
			this->D.S[i] = (1  - std::sqrt(this->D.S[K-1] / this->D.S[i])) / this->D.S[i];
		}
		for(int i=K;i<D.k;i++)
			this->D.S[i] = 0;
	}
	
	this->D.k--;


	int batchsize = 64;
	std::cout << "Stepsize: "<<eta << " Batchsize: "<< batchsize << std::endl;
	deleteDenseMatrix(kernelM);
	makeDenseMatrix(kernelM,batchsize,N);
	
	int* minibatch   = new int[batchsize];
	double* gradient = new double[batchsize*numberOfLabels]; //batchsize x numLabels
	double* fullGradBuffer = new double[N*numberOfLabels];
	double* lowRankBuffer = new double[K*numberOfLabels];
	double* nystroemBuffer = 0;
	if(nystroemSampling)
		nystroemBuffer = new double[this->Khat.k*numberOfLabels];

	std::cout << "Setup Time "<< std::chrono::duration_cast<std::chrono::seconds>( std::chrono::high_resolution_clock::now() - timeMeasurement ).count() << std::endl;

	for(int epoch=0;epoch<numberOfEpoches;epoch++)
	{
		std::cout << "Epoch "<< epoch << std::endl;
		timeMeasurement = std::chrono::high_resolution_clock::now();
		epoche(N,K,d,batchsize,numberOfLabels,kernelM,X,Y,
			indices,minibatch,this->alpha,gradient,fullGradBuffer,lowRankBuffer,
			this->D, eta,kernel,this->hyper,
			this->Khat,nystroemBuffer,nystroemSampling,conditioning,
			loss);
		std::cout << " Norm: " << this->hilbertNorm() << std::endl;
		std::cout << "Epoche duration "<< std::chrono::duration_cast<std::chrono::seconds>( std::chrono::high_resolution_clock::now() - timeMeasurement ).count() << " seconds " << std::endl;
		if(valN>0 && (epoch%validateEvery == (validateEvery-1) || epoch==numberOfEpoches-1))
		{
			predict(valX,validationBuffer,valN,d);
			double accuracy = 0;
			for(int i=0;i<valN;i++)
				if(validationBuffer[i]==valY[i])
					accuracy+=1.0/valN;
			std::cout << "Validation Accuracy "<< accuracy << std::endl;
		}
	}
	if(nystroemSampling)
	{
		delete[] nystroemBuffer;
	}
	if(valN>0)
		delete[] validationBuffer;
	delete[] fullGradBuffer;
	delete[] lowRankBuffer;	
	delete[] indices;
	delete[] sample;	
}

void KernelMachine::predict(double* X, int* Y, int N, int d)
{
	int* indices = new int[this->N];
	int numberOfLabels = this->numberOfLabels;
	for(int i=0;i<this->N;i++)
		indices[i]=i;
	int batchsize = 256;
	if(batchsize>N)
		batchsize=N;
	DenseMatrix kernelM;
	
	double* nystroemAlpha = NULL;
	if(this->nystroemSampling)
	{
		makeDenseMatrix(kernelM,batchsize,this->sampleSize);
		nystroemAlpha = new double[this->sampleSize * this->numberOfLabels];
		cblas_dgemm(CblasColMajor,
					CblasNoTrans,
					CblasNoTrans,
					this->sampleSize,         //M
					this->numberOfLabels,     //N
					this->Khat.k,             //K
					1.0,                      //alpha
					this->nystroemMap,        //A
					this->sampleSize,         //LDA
					this->alpha,              //B
					this->Khat.k,             //LDB
					0.0,                      //beta
					nystroemAlpha,            //C
					this->sampleSize          //LDC
				);
	}
	else
	{
		makeDenseMatrix(kernelM,batchsize,this->N);
	}
	int* minibatch   = new int[batchsize];
	double* gradient = new double[batchsize*numberOfLabels]; //batchsize x numLabels
	for(int firstExample=0;firstExample<N;firstExample+=batchsize)
	{
		int broken = 0;
		if(firstExample+batchsize>N)
		{
			broken = firstExample-N+batchsize;
			firstExample=N-batchsize;
		}
		for(int j=0;j<batchsize;j++)
			minibatch[j]=firstExample+j;

		if(!this->nystroemSampling)
		{
			this->kernel(kernelM,X,this->centers,minibatch,indices,this->d,0);
			cblas_dgemm(CblasColMajor,
				CblasNoTrans,
				CblasNoTrans,
				batchsize,
				numberOfLabels,
				this->N,
				1.0,
				kernelM.X,
				batchsize,
				this->alpha,
				this->N,
				0.0,
				gradient,
				batchsize
			);
		}
		else
		{
			this->kernel(kernelM,X,this->centers,minibatch,indices,this->d,0);
			cblas_dgemm(CblasColMajor,
				CblasNoTrans,
				CblasNoTrans,
				batchsize,
				numberOfLabels,
				this->sampleSize,
				1.0,
				kernelM.X,
				batchsize,
				nystroemAlpha,
				this->sampleSize,
				0.0,
				gradient,
				batchsize
			);
		}
		for(int i=0;i<batchsize;i++)
		{
			int largestClass = 0;
			double largestClassStatistic = -1000000.0;

			for(int l=0;l<numberOfLabels;l++)
			{			
				if(gradient[l*batchsize+i]>largestClassStatistic)
				{
					largestClass = l;
					largestClassStatistic = gradient[l*batchsize+i];
				}
			}
			Y[minibatch[i]] = largestClass;
		}
	}
	delete[] indices;
	delete[] gradient;
	delete[] minibatch;
	deleteDenseMatrix(kernelM);
	if(this->nystroemSampling)
		delete[] nystroemAlpha;
}

void KernelMachine::predictStatistics(double* X, double* Y, int N, int d)
{
	int* indices = new int[this->N];
	int numberOfLabels = this->numberOfLabels;
	for(int i=0;i<this->N;i++)
		indices[i]=i;
	int batchsize = 256;
	if(batchsize>N)
		batchsize=N;
	DenseMatrix kernelM;
	
	double* nystroemAlpha = NULL;
	if(this->nystroemSampling)
	{
		makeDenseMatrix(kernelM,batchsize,this->sampleSize);
		nystroemAlpha = new double[this->sampleSize * this->numberOfLabels];
		cblas_dgemm(CblasColMajor,
					CblasNoTrans,
					CblasNoTrans,
					this->sampleSize,         //M
					this->numberOfLabels,     //N
					this->Khat.k,             //K
					1.0,                      //alpha
					this->nystroemMap,        //A
					this->sampleSize,         //LDA
					this->alpha,              //B
					this->Khat.k,             //LDB
					0.0,                      //beta
					nystroemAlpha,            //C
					this->sampleSize          //LDC
				);
	}
	else
	{
		makeDenseMatrix(kernelM,batchsize,this->N);
	}
	int* minibatch   = new int[batchsize];
	// double* gradient = new double[batchsize*numberOfLabels]; //batchsize x numLabels
	for(int firstExample=0;firstExample<N;firstExample+=batchsize)
	{
		int broken = 0;
		if(firstExample+batchsize>N)
		{
			broken = firstExample-N+batchsize;
			firstExample=N-batchsize;
		}
		for(int j=0;j<batchsize;j++)
			minibatch[j]=firstExample+j;

		if(!this->nystroemSampling)
		{
			this->kernel(kernelM,X,this->centers,minibatch,indices,this->d,0);
			cblas_dgemm(CblasColMajor,
				CblasNoTrans,
				CblasNoTrans,
				batchsize,
				numberOfLabels,
				this->N,
				1.0,
				kernelM.X,
				batchsize,
				this->alpha,
				this->N,
				0.0,
				&Y[firstExample],
				N
			);
		}
		else
		{
			this->kernel(kernelM,X,this->centers,minibatch,indices,this->d,0);
			cblas_dgemm(CblasColMajor,
				CblasNoTrans,
				CblasNoTrans,
				batchsize,
				numberOfLabels,
				this->sampleSize,
				1.0,
				kernelM.X,
				batchsize,
				nystroemAlpha,
				this->sampleSize,
				0.0,
				&Y[firstExample],
				N
			);
		}
	}
	delete[] indices;
	delete[] minibatch;
	deleteDenseMatrix(kernelM);
	if(this->nystroemSampling)
		delete[] nystroemAlpha;
}