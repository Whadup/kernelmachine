#pragma once
#include <vector>
#include "matrix.h"
class KernelMachine
{
public:
	int N;
	int sampleSize;
	int d;
	int numberOfLabels;
	bool nystroemSampling;
	double* centers = 0;
	double* alpha = 0;
	void(*kernel)(DenseMatrix&,double*,double*, int*, int*, int, void*) = 0;
	void* hyper = 0;
	LowRankMatrix Khat;
	LowRankMatrix D;
	double* nystroemMap = 0;
	void estimateFirstEigenvalues();
// public:
	virtual ~KernelMachine();
	// void fit(const std::vector< std::vector<double> > & X,const std::vector<int>& Y);
	void fit(double* X, int* Y, int N, int d, double* valX, int* valY, int valN,
			int numberOfEpoches, int K, int sampleSize,int kernelId,int lossId, bool nystroem, bool conditioning, bool exactConditioning);
	// void nystroem(double* X, int* Y, int N, int d);
	void predict(double* X, int* Y, int N, int d);
	void predictStatistics(double* X, double* Y, int N, int d);
	double hilbertNorm();
	double l2Norm();
};