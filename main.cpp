#include "kernelmachine.h"
#include <random>
#include <iostream>
#include <math.h>
#include <fstream>
int main(){
	int N = 10000;
	double* X = new double[N*2];
	int* Y = new int[N];
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(-1,1);
	for(int i=0;i<N;i++)
	{
		X[i*2] = dis(gen)*0.2+0.8;
		X[i*2+1] = dis(gen)*0.5+0.5;
		Y[i] = abs(((int)floor(pow(10,X[i*2])- pow(10,X[i*2+1])))%2);
		X[i*2] -= 0.8;
		X[i*2+1] -= 0.5;
		X[i*2]/=(0.2*std::sqrt(2));
		X[i*2+1]/=(0.5*std::sqrt(2));
		// std::cout << X[i*2] << ","<<X[1+i*2] << "," << Y[i] << std::endl;

	}
	std::ofstream ofs2("/Users/pfahler/Desktop/debug.csv", std::ofstream::out);
	for(int i=0;i<N;i++)
		ofs2 << X[2*i]<<"," << X[2*i+1] << "," << Y[i] << std::endl;
	ofs2.close();

	double* testX = new double[2*100*100];
	int* testY = new int[100*100];
	for(int x=0;x<100;x++)
	{	for(int y=0;y<100;y++)
		{
			testX[2*(y+100*x)] = -1.0/std::sqrt(2)+0.5/100 + 2.0/std::sqrt(2)*(x/100.0);
			testX[2*(y+100*x)+1] = -1.0/std::sqrt(2)+0.5/100 + 2.0/std::sqrt(2)*(y/100.0);
			// std::cout << X[2*(y+100*x)] <<"," <<X[2*(y+100*x)+1] << std::endl;
		}
	}

	KernelMachine k;
	k.fit(X,Y,N/2,2,&X[N], &Y[N/2], N/2,100,5000,5000,3,2,true,true,true);
	
	
	k.predict(testX,testY,100*100,2);
	std::ofstream ofs ("/Users/pfahler/Desktop/result.csv", std::ofstream::out);
	for(int i=0;i<100*100;i++)
		ofs << testX[2*i]<<"," << testX[2*i+1] << "," << testY[i] << std::endl;
	ofs.close();
	return 0;
}