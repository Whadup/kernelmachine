#pragma once
#include <vector>
struct SparseMatrix
{
	std::vector<int> documentSizes;
	std::vector<int> tokenIds;
	std::vector<double> tokenFrequencies;

	std::vector<int> tokenSizes;
	std::vector<int> documentIds;
	std::vector<double> documentFrequencies;

	double* zero;

};

struct LowRankMatrix
{
	double* U=0;
	double* S=0;
	double* V=0;
	int m;
	int n;
	int k;
	double* gU=0;
	double* gS=0;
	double* gV=0;
};

struct DenseMatrix
{
	double* X=0;
	int m;
	int n;
	double* gX=0;
};

void duplicateSparseMatrix(SparseMatrix& orig, SparseMatrix& copy);
void makeLowRankMatrix(LowRankMatrix& l, int m, int n, int k);
void makeSymmetricLowRankMatrix(LowRankMatrix& l, int m, int k);
void deleteLowRankMatrix(LowRankMatrix& l);

void sparseMatrixVector(SparseMatrix& s,double* x, double* y,bool transposed,bool add);
void lowRankMatrixVector(LowRankMatrix& s,double scale,double* x, double* y,bool transposed,bool add);
void denseMatrixVector(DenseMatrix& s,double* x, double* y,bool transposed,bool add);
void reTranspose(int ii,int nwords,SparseMatrix& d);

double logSum(LowRankMatrix& l, int doc,double* buffer, double& minValue);
double sampledLogSum(LowRankMatrix& l, int doc,double* buffer, double& minValue,int sampleSize, int* sample);

double logSumAccelerated(LowRankMatrix& l, LowRankMatrix& l2, double a,double b,int doc,double* buffer, double& minValue);
double sampledLogSumAccelerated(LowRankMatrix& l, LowRankMatrix& l2, double a,double b, int doc,double* buffer, double& minValue,int sampleSize, int* sample);

double lowRankEntry(LowRankMatrix& l, int token, int doc);

void makeDenseMatrix(DenseMatrix& x, int m, int l);
void deleteDenseMatrix(DenseMatrix& x);

void push(DenseMatrix& x);
void pull(DenseMatrix& x);