### CPU specific flags for gcc/gfortran.
#CPUOPT = 

# Optimization flags
OPT = -nofor_main -O3 -funroll-all-loops  $(CPUOPT) 
# OPT = -O0 -funroll-all-loops -ffast-math $(CPUOPT) 
#-floop-parallelize-all -ftree-parallelize-loops=10
OPT_ACCURATE = -O3 -funroll-all-loops $(CPUOPT)
NO_OPT = -O0

# Compiler flags
FFLAGS = -g $(OPT)
FFLAGS_NOOPT = -g  -fno-second-underscore -Wall $(NOOPT) 
FFLAGS_ACCURATE = -g  -fno-second-underscore  -Wall $(OPT_ACCURATE)
CFLAGS = -g   -Wall  $(OPT) 
LINKFLAGS = -g   -Wall  $(OPT) 

# Which compiler and linker to use
CC = icc
F77 = ifort 
LINKER = ifort 
MAKELIB = ar rc

# Linker paths and flags
LIBPATH =  -L/usr/lib
BLAS = -mkl
RANLIB = ranlib
MGS = mgs.pentium

#  Platform postfix to use in naming executables and library files
PLAT = LINUX_GCC
