### CPU specific flags for gcc/gfortran.
#CPUOPT = 

# Optimization flags
OPT = -O3 -funroll-all-loops -ffast-math $(CPUOPT)
#-floop-parallelize-all -ftree-parallelize-loops=10
OPT_ACCURATE = -O3 -funroll-all-loops $(CPUOPT)
NO_OPT = -O0

# Compiler flags
FFLAGS = -g -fPIC -fno-second-underscore  -W  $(OPT)
FFLAGS_NOOPT = -g -fPIC -fno-second-underscore -W -Wall $(NOOPT) 
FFLAGS_ACCURATE = -g -fPIC  -fno-second-underscore -W -Wall $(OPT_ACCURATE)
CFLAGS = -fPIC -g  -W -Wall  $(OPT) 
LINKFLAGS = -g -fPIC -W -Wall  $(OPT) 

# Which compiler and linker to use
CC = gcc
F77 = gfortran 
LINKER = gfortran 
MAKELIB = ar rc

# Linker paths and flags
LIBPATH =  -L/usr/lib
BLAS = -lblas
RANLIB = ranlib
MGS = mgs.pentium

#  Platform postfix to use in naming executables and library files
PLAT = LINUX_GCC
