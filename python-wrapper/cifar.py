import keras
from keras.datasets import cifar10
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
def load(greyscale=False,integer_labels=False):
	num_classes = 10	    # number of classes
	(x_train, y_train), (x_test, y_test) = cifar10.load_data()
	datagen = ImageDataGenerator(
		featurewise_center=False,  # set input mean to 0 over the dataset
		samplewise_center=False,  # set each sample mean to 0
		featurewise_std_normalization=False,  # divide inputs by std of the dataset
		samplewise_std_normalization=False,  # divide each input by its std
		zca_whitening=False,  # apply ZCA whitening
		rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
		width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
		height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
		horizontal_flip=True,  # randomly flip images
		vertical_flip=False)  # randomly flip images

	# Compute quantities required for feature-wise normalization
	# (std, mean, and principal components if ZCA whitening is applied).
	datagen.fit(x_train)
	gen = datagen.flow(x_train,y_train,batch_size=1)
	x_train = []
	y_train = []
	for i in range(500000):
		z = next(gen)
		x_train.append(z[0][0])
		y_train.append(z[1][0])
	x_train = np.array(x_train)
	y_train = np.array(y_train)
	print(x_train.shape[0], 'train samples')
	print(x_test.shape[0], 'test samples')
	print(type(x_train))
	if(greyscale):
		x_train = np.sum(x_train,axis=3)
		x_test = np.sum(x_test,axis=3)
	x_train = x_train.reshape((x_train.shape[0],-1))
	x_test = x_test.reshape((x_test.shape[0],-1))
	print('x_train shape:', x_train.shape)
	# print(x_train[0],x_train.dtype)
	n, D = x_train.shape    # (n_sample, n_feature)
	x_train = np.divide(x_train,255.0)
	x_test = np.divide(x_test,255.0)
	
	print("Load CIFAR10 dataset.")
	print(x_train.shape[0], 'train samples')
	print(x_test.shape[0], 'test samples')

	d = np.int32(n / 2) * 2 # number of random features
	x_train = keras.utils.normalize(x_train,axis=1)
	x_test = keras.utils.normalize(x_test,axis=1)
	# convert class vectors to binary class matrices
	print(y_train.shape)
	if not integer_labels:
		y_train = keras.utils.to_categorical(y_train, num_classes)
		y_test = keras.utils.to_categorical(y_test, num_classes)
	else:
		y_train = y_train.reshape(y_train.shape[0])
		y_test = y_test.reshape(y_test.shape[0])
	R = np.max(np.linalg.norm(x_train,2,axis=1))**2
	return num_classes,x_train,x_test,y_train,y_test,n,D,R,d