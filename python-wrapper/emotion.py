

from sklearn.model_selection import train_test_split
import numpy as np

def scale_linear_bycolumn(X, high=1.0, low=0.0):
    mins = np.min(X, axis=0)
    maxs = np.max(X, axis=0)+0.00000001
    rng = maxs - mins
    return high - (((high - low) * (maxs - X)) / rng)

def load(greyscale=True,integer_labels=False):
	num_classes = 6	    # number of classes
	x_train = np.genfromtxt("/data/pfahler/emotion/train_embedded.csv", delimiter=' ')
	x_test = np.genfromtxt("/data/pfahler/emotion/trial_embedded.csv", delimiter=' ')
	y_train = []
	y_test = []
	with open("/data/pfahler/emotion/train_lower.csv","r") as f:
		for l in f:
			y_train.append(l.split("\t")[0].strip())
	with open("/data/pfahler/emotion/trial_lower.csv","r") as f:
		for l in f:
			y_test.append(l.split("\t")[0].strip())
	mapper = {"__label__joy":0,
	"__label__anger":1,
	"__label__disgust":2 ,
	"__label__fear":3,
	"__label__surprise":4,
	"__label__sad":5}
	y_train = np.array([mapper[x] for x in y_train])
	y_test = np.array([mapper[x] for x in y_test])
	# print(x_train[0],x_train.dtype)
	n, D = x_train.shape    # (n_sample, n_feature)
	

	d = np.int32(n / 2) * 2 # number of random features
	#x_train = keras.utils.normalize(x_train,axis=1)
	#x_test = keras.utils.normalize(x_test,axis=1)
	# convert class vectors to binary class matrices
	if not integer_labels:
		pass

	R = np.max(np.linalg.norm(x_train,2,axis=1))**2
	return num_classes,x_train,x_test,y_train,y_test,n,D,R,d	

