#!/usr/bin/env python3
import photon_stream
import csv,operator,sys,os
import numpy as np
import os
import json
import timeit
from sklearn.model_selection import train_test_split
from random import shuffle
def scale_linear_bycolumn(X, high=1.0, low=0.0):
	mins = np.min(X)
	maxs = np.max(X)+0.00000001
	rng = maxs - mins
	return high - (((high - low) * (maxs - X)) / rng)

def threshold(a,b):
	if a<b:
		return 0
	return a-b

dob =  None
import pickle

class MacOSFile(object):

    def __init__(self, f):
        self.f = f

    def __getattr__(self, item):
        return getattr(self.f, item)

    def read(self, n):
        # print("reading total_bytes=%s" % n, flush=True)
        if n >= (1 << 31):
            buffer = bytearray(n)
            idx = 0
            while idx < n:
                batch_size = min(n - idx, 1 << 31 - 1)
                # print("reading bytes [%s,%s)..." % (idx, idx + batch_size), end="", flush=True)
                buffer[idx:idx + batch_size] = self.f.read(batch_size)
                # print("done.", flush=True)
                idx += batch_size
            return buffer
        return self.f.read(n)

    def write(self, buffer):
        n = len(buffer)
        print("writing total_bytes=%s..." % n, flush=True)
        idx = 0
        while idx < n:
            batch_size = min(n - idx, 1 << 31 - 1)
            print("writing bytes [%s, %s)... " % (idx, idx + batch_size), end="", flush=True)
            self.f.write(buffer[idx:idx + batch_size])
            print("done.", flush=True)
            idx += batch_size


def pickle_dump(obj, file_path):
    with open(file_path, "wb") as f:
        return pickle.dump(obj, MacOSFile(f), protocol=pickle.HIGHEST_PROTOCOL)


def pickle_load(file_path):
    with open(file_path, "rb") as f:
        return pickle.load(MacOSFile(f))

def load(start,end,th,integer_labels=True):
	global dob
	X = []
	Y = []
	thresholder = np.vectorize(threshold)
	if dob is None:
		if os.path.isfile("/data/pfahler/C3/dob.pickle"):			
			dob=pickle_load("/data/pfahler/C3/dob.pickle")
			for s,y in dob:
				X.append(np.fft.fft(thresholder(s[start:end].max(axis=0),th)))
				# X.append(thresholder(s[start:end].sum(axis=0),th))
				Y.append(y)
		else:
			dob=[]
			print("Reading Gamma data")
			names=os.listdir("/data/pfahler/C3/gamma/")
			names=['012120.phs.jsonl.gz', '013978.phs.jsonl.gz', '013949.phs.jsonl.gz', '014192.phs.jsonl.gz', '013836.phs.jsonl.gz', '011726.phs.jsonl.gz', '014751.phs.jsonl.gz', '013307.phs.jsonl.gz', '012873.phs.jsonl.gz', '014714.phs.jsonl.gz', '011999.phs.jsonl.gz', '012276.phs.jsonl.gz', '013464.phs.jsonl.gz', '012592.phs.jsonl.gz', '012272.phs.jsonl.gz', '012300.phs.jsonl.gz', '013463.phs.jsonl.gz', '012981.phs.jsonl.gz', '011037.phs.jsonl.gz', '012030.phs.jsonl.gz', '013383.phs.jsonl.gz', '014423.phs.jsonl.gz', '011694.phs.jsonl.gz', '014758.phs.jsonl.gz', '014513.phs.jsonl.gz', '013642.phs.jsonl.gz', '012536.phs.jsonl.gz', '011377.phs.jsonl.gz', '012016.phs.jsonl.gz', '014050.phs.jsonl.gz', '012712.phs.jsonl.gz', '012036.phs.jsonl.gz', '014273.phs.jsonl.gz', '014658.phs.jsonl.gz', '012040.phs.jsonl.gz', '013616.phs.jsonl.gz', '013482.phs.jsonl.gz', '012280.phs.jsonl.gz', '011107.phs.jsonl.gz', '011362.phs.jsonl.gz', '014091.phs.jsonl.gz', '014362.phs.jsonl.gz', '011225.phs.jsonl.gz', '013827.phs.jsonl.gz', '012898.phs.jsonl.gz', '012116.phs.jsonl.gz', '012033.phs.jsonl.gz', '013143.phs.jsonl.gz', '013227.phs.jsonl.gz', '011291.phs.jsonl.gz', '011613.phs.jsonl.gz', '013581.phs.jsonl.gz', '011618.phs.jsonl.gz', '012677.phs.jsonl.gz', '012621.phs.jsonl.gz', '012551.phs.jsonl.gz', '013521.phs.jsonl.gz', '012881.phs.jsonl.gz', '012268.phs.jsonl.gz', '012314.phs.jsonl.gz', '010956.phs.jsonl.gz', '011873.phs.jsonl.gz', '011671.phs.jsonl.gz', '014057.phs.jsonl.gz', '010911.phs.jsonl.gz', '012903.phs.jsonl.gz', '014172.phs.jsonl.gz', '014558.phs.jsonl.gz', '013272.phs.jsonl.gz', '013617.phs.jsonl.gz', '014355.phs.jsonl.gz', '014235.phs.jsonl.gz', '012108.phs.jsonl.gz', '012253.phs.jsonl.gz', '014253.phs.jsonl.gz', '014335.phs.jsonl.gz', '014583.phs.jsonl.gz', '013962.phs.jsonl.gz', '011261.phs.jsonl.gz', '012765.phs.jsonl.gz', '013022.phs.jsonl.gz', '013638.phs.jsonl.gz', '011561.phs.jsonl.gz', '013621.phs.jsonl.gz', '013418.phs.jsonl.gz', '011565.phs.jsonl.gz', '011637.phs.jsonl.gz', '013862.phs.jsonl.gz', '011468.phs.jsonl.gz', '013493.phs.jsonl.gz', '012913.phs.jsonl.gz', '014735.phs.jsonl.gz', '014252.phs.jsonl.gz', '014336.phs.jsonl.gz', '014818.phs.jsonl.gz', '011190.phs.jsonl.gz', '013456.phs.jsonl.gz', '010928.phs.jsonl.gz', '013016.phs.jsonl.gz', '012377.phs.jsonl.gz']
			for f in names[:40]:
				if not f[-3:]==".gz":
					continue
				stream =  photon_stream.EventListReader("/data/pfahler/C3/gamma/"+f)
				for s in stream:
					dob.append((s.photon_stream.image_sequence,0))
					X.append(thresholder(s.photon_stream.image_sequence[start:end].sum(axis=0),th))
					Y.append(0)
			print("Reading Proton data")
			names=os.listdir("/data/pfahler/C3/proton/")
			names=['026892.phs.jsonl.gz', '026358.phs.jsonl.gz', '028278.phs.jsonl.gz', '065676.phs.jsonl.gz', '028449.phs.jsonl.gz', '066496.phs.jsonl.gz', '067613.phs.jsonl.gz', '067140.phs.jsonl.gz', '026183.phs.jsonl.gz', '067473.phs.jsonl.gz', '028605.phs.jsonl.gz', '026780.phs.jsonl.gz', '067714.phs.jsonl.gz', '028414.phs.jsonl.gz', '026828.phs.jsonl.gz', '066842.phs.jsonl.gz', '027121.phs.jsonl.gz', '026403.phs.jsonl.gz', '026930.phs.jsonl.gz', '027542.phs.jsonl.gz', '067488.phs.jsonl.gz', '026331.phs.jsonl.gz', '028057.phs.jsonl.gz', '025330.phs.jsonl.gz', '027779.phs.jsonl.gz', '026545.phs.jsonl.gz', '025938.phs.jsonl.gz', '027369.phs.jsonl.gz', '067896.phs.jsonl.gz', '026657.phs.jsonl.gz', '028397.phs.jsonl.gz', '028603.phs.jsonl.gz', '067347.phs.jsonl.gz', '064484.phs.jsonl.gz', '066929.phs.jsonl.gz', '027864.phs.jsonl.gz', '067275.phs.jsonl.gz', '025917.phs.jsonl.gz', '066408.phs.jsonl.gz', '066201.phs.jsonl.gz', '028017.phs.jsonl.gz', '066192.phs.jsonl.gz', '067376.phs.jsonl.gz', '027325.phs.jsonl.gz', '028433.phs.jsonl.gz', '026252.phs.jsonl.gz', '025934.phs.jsonl.gz', '067156.phs.jsonl.gz', '025788.phs.jsonl.gz', '027724.phs.jsonl.gz', '065326.phs.jsonl.gz', '067085.phs.jsonl.gz', '028211.phs.jsonl.gz', '065400.phs.jsonl.gz', '064941.phs.jsonl.gz', '027544.phs.jsonl.gz', '065222.phs.jsonl.gz', '068402.phs.jsonl.gz', '025100.phs.jsonl.gz', '027101.phs.jsonl.gz', '027537.phs.jsonl.gz', '026849.phs.jsonl.gz', '025335.phs.jsonl.gz', '068009.phs.jsonl.gz', '027219.phs.jsonl.gz', '064874.phs.jsonl.gz', '066072.phs.jsonl.gz', '027963.phs.jsonl.gz', '065827.phs.jsonl.gz', '025109.phs.jsonl.gz', '066258.phs.jsonl.gz', '026588.phs.jsonl.gz', '068026.phs.jsonl.gz', '027760.phs.jsonl.gz', '026900.phs.jsonl.gz', '064798.phs.jsonl.gz', '064504.phs.jsonl.gz', '025355.phs.jsonl.gz', '064555.phs.jsonl.gz', '027920.phs.jsonl.gz', '027220.phs.jsonl.gz', '025397.phs.jsonl.gz', '027735.phs.jsonl.gz', '065385.phs.jsonl.gz', '065088.phs.jsonl.gz', '067731.phs.jsonl.gz', '067737.phs.jsonl.gz', '027928.phs.jsonl.gz', '028298.phs.jsonl.gz', '067906.phs.jsonl.gz', '028695.phs.jsonl.gz', '026691.phs.jsonl.gz', '064424.phs.jsonl.gz', '067018.phs.jsonl.gz', '067993.phs.jsonl.gz', '066679.phs.jsonl.gz', '025776.phs.jsonl.gz', '067937.phs.jsonl.gz', '067916.phs.jsonl.gz', '067888.phs.jsonl.gz', '026963.phs.jsonl.gz', '065611.phs.jsonl.gz', '026127.phs.jsonl.gz', '028337.phs.jsonl.gz', '027717.phs.jsonl.gz', '066307.phs.jsonl.gz', '067735.phs.jsonl.gz', '027571.phs.jsonl.gz', '067634.phs.jsonl.gz', '065188.phs.jsonl.gz', '027716.phs.jsonl.gz', '067268.phs.jsonl.gz', '067038.phs.jsonl.gz', '066207.phs.jsonl.gz', '066176.phs.jsonl.gz', '065547.phs.jsonl.gz', '066255.phs.jsonl.gz', '027926.phs.jsonl.gz', '025599.phs.jsonl.gz', '026375.phs.jsonl.gz', '066748.phs.jsonl.gz', '067853.phs.jsonl.gz', '026211.phs.jsonl.gz', '066215.phs.jsonl.gz', '025786.phs.jsonl.gz', '028118.phs.jsonl.gz', '067897.phs.jsonl.gz', '064578.phs.jsonl.gz', '065195.phs.jsonl.gz', '067036.phs.jsonl.gz', '028818.phs.jsonl.gz', '028287.phs.jsonl.gz', '066905.phs.jsonl.gz', '067158.phs.jsonl.gz', '068116.phs.jsonl.gz', '025706.phs.jsonl.gz', '025192.phs.jsonl.gz', '065822.phs.jsonl.gz', '066902.phs.jsonl.gz', '067483.phs.jsonl.gz', '068189.phs.jsonl.gz', '066603.phs.jsonl.gz', '027371.phs.jsonl.gz', '028332.phs.jsonl.gz', '027670.phs.jsonl.gz', '065737.phs.jsonl.gz', '025394.phs.jsonl.gz', '066864.phs.jsonl.gz', '065396.phs.jsonl.gz', '067056.phs.jsonl.gz', '025140.phs.jsonl.gz', '026096.phs.jsonl.gz', '027526.phs.jsonl.gz', '064636.phs.jsonl.gz', '065623.phs.jsonl.gz', '065280.phs.jsonl.gz', '067514.phs.jsonl.gz', '025167.phs.jsonl.gz', '064899.phs.jsonl.gz', '025839.phs.jsonl.gz', '064475.phs.jsonl.gz', '026057.phs.jsonl.gz', '065480.phs.jsonl.gz', '027112.phs.jsonl.gz', '027362.phs.jsonl.gz', '067046.phs.jsonl.gz', '026078.phs.jsonl.gz', '027261.phs.jsonl.gz', '066919.phs.jsonl.gz', '066170.phs.jsonl.gz', '066897.phs.jsonl.gz', '064518.phs.jsonl.gz', '067184.phs.jsonl.gz', '068042.phs.jsonl.gz', '066189.phs.jsonl.gz', '068338.phs.jsonl.gz', '027531.phs.jsonl.gz', '065454.phs.jsonl.gz', '026862.phs.jsonl.gz', '028778.phs.jsonl.gz', '027385.phs.jsonl.gz', '027430.phs.jsonl.gz', '025907.phs.jsonl.gz', '025976.phs.jsonl.gz', '025323.phs.jsonl.gz', '026301.phs.jsonl.gz', '068001.phs.jsonl.gz', '067210.phs.jsonl.gz', '065346.phs.jsonl.gz', '025535.phs.jsonl.gz', '026679.phs.jsonl.gz', '064937.phs.jsonl.gz', '026589.phs.jsonl.gz', '024977.phs.jsonl.gz', '026035.phs.jsonl.gz', '067563.phs.jsonl.gz', '066098.phs.jsonl.gz', '026090.phs.jsonl.gz', '065677.phs.jsonl.gz', '067828.phs.jsonl.gz']
			#names=['026377.phs.jsonl.gz', '067522.phs.jsonl.gz', '064824.phs.jsonl.gz', '067539.phs.jsonl.gz', '025539.phs.jsonl.gz', '025593.phs.jsonl.gz', '028499.phs.jsonl.gz', '027874.phs.jsonl.gz', '026910.phs.jsonl.gz', '025916.phs.jsonl.gz', '065835.phs.jsonl.gz', '066501.phs.jsonl.gz', '028300.phs.jsonl.gz', '027477.phs.jsonl.gz', '026087.phs.jsonl.gz', '065919.phs.jsonl.gz', '064924.phs.jsonl.gz', '028337.phs.jsonl.gz', '025956.phs.jsonl.gz', '065311.phs.jsonl.gz', '027177.phs.jsonl.gz', '068190.phs.jsonl.gz', '028453.phs.jsonl.gz', '027727.phs.jsonl.gz', '027559.phs.jsonl.gz', '066276.phs.jsonl.gz', '068365.phs.jsonl.gz', '067077.phs.jsonl.gz', '026721.phs.jsonl.gz', '066992.phs.jsonl.gz', '067427.phs.jsonl.gz', '026685.phs.jsonl.gz', '065221.phs.jsonl.gz', '067941.phs.jsonl.gz', '026635.phs.jsonl.gz', '064657.phs.jsonl.gz', '064480.phs.jsonl.gz', '026178.phs.jsonl.gz', '067999.phs.jsonl.gz', '065783.phs.jsonl.gz', '068041.phs.jsonl.gz', '065464.phs.jsonl.gz', '064603.phs.jsonl.gz', '065339.phs.jsonl.gz', '027349.phs.jsonl.gz', '064725.phs.jsonl.gz', '028726.phs.jsonl.gz', '064652.phs.jsonl.gz', '066275.phs.jsonl.gz', '064560.phs.jsonl.gz', '065090.phs.jsonl.gz', '067741.phs.jsonl.gz', '026023.phs.jsonl.gz', '067995.phs.jsonl.gz', '027234.phs.jsonl.gz', '025386.phs.jsonl.gz', '025509.phs.jsonl.gz', '026482.phs.jsonl.gz', '065662.phs.jsonl.gz', '026732.phs.jsonl.gz', '064945.phs.jsonl.gz', '026634.phs.jsonl.gz', '065594.phs.jsonl.gz', '028616.phs.jsonl.gz', '027592.phs.jsonl.gz', '067386.phs.jsonl.gz', '065615.phs.jsonl.gz', '065295.phs.jsonl.gz', '027429.phs.jsonl.gz', '065876.phs.jsonl.gz', '025615.phs.jsonl.gz', '025241.phs.jsonl.gz', '025230.phs.jsonl.gz', '067275.phs.jsonl.gz', '026276.phs.jsonl.gz', '065904.phs.jsonl.gz', '065525.phs.jsonl.gz', '028027.phs.jsonl.gz', '027397.phs.jsonl.gz', '067315.phs.jsonl.gz', '027497.phs.jsonl.gz', '066082.phs.jsonl.gz', '028881.phs.jsonl.gz', '065747.phs.jsonl.gz', '027934.phs.jsonl.gz', '065466.phs.jsonl.gz', '064495.phs.jsonl.gz', '026645.phs.jsonl.gz', '068220.phs.jsonl.gz', '026828.phs.jsonl.gz', '026398.phs.jsonl.gz', '064949.phs.jsonl.gz', '026589.phs.jsonl.gz', '027094.phs.jsonl.gz', '067531.phs.jsonl.gz', '067378.phs.jsonl.gz', '068395.phs.jsonl.gz', '025316.phs.jsonl.gz', '065539.phs.jsonl.gz', '026621.phs.jsonl.gz']
			for f in names:
						if f[-3:]!=".gz":
								continue
						stream =  photon_stream.EventListReader("/data/pfahler/C3/proton/"+f)
						for s in stream:
							X.append(thresholder(s.photon_stream.image_sequence[start:end].sum(axis=0),th))
							dob.append((s.photon_stream.image_sequence,1))
							Y.append(1)
			pickle_dump(dob,"/data/pfahler/C3/dob.pickle")
	else:
		for s,y in dob:
			X.append(np.fft.fft(thresholder(s[start:end].max(axis=0),th)))
			Y.append(y)

	#print("Dropped", dropped, "data points because of NaN")
	#print(len(X), "data points still available")
	# np.savetxt("/data/pfahler/C3/X.csv",np.array(X))
	# np.savetxt("/data/pfahler/C3/Y.csv",np.array(Y))

	x_train,x_test,y_train,y_test = train_test_split(scale_linear_bycolumn(np.array(X)), np.array(Y), random_state=1337,test_size=0.25)
	num_classes = 2
	n, D = x_train.shape
	d = np.int32(n / 2) * 2 # number of random features
	R = np.max(np.linalg.norm(x_train,2,axis=1))**2
	#for x in x_train[0]:
	#	print(x)
	return num_classes,x_train,x_test,y_train,y_test,n,D,R,d

