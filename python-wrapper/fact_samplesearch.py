# Python running example
from sacred import Experiment
# from __future__ import print_function

import numpy as np
from kernelmachine import PyKernelMachine
import fact_sample
# Initialising the wrapped c++ function
import sys,os
from sacred.observers import MongoObserver,FileStorageObserver
from sklearn.metrics import roc_auc_score,classification_report,confusion_matrix
ex = Experiment()

def parseStdOut(text,run):
	rules = [
		("Accuracy: (?P<acc>[0-9.]+) Loss: (?P<loss>[0-9.]+)",[("acc","Accuracy"),("loss","Loss")]),
		("Epoche duration (?P<secs>[0-9]+) seconds",[("secs","Epoche Time")]),
		("Setup Time (?P<secs>[0-9]+)$",[("secs","Setup Time")]),
		("Validation Accuracy (?P<acc>[0-9.]+)$",[("acc","Validation Accuracy")])

	]
	import re
	if not hasattr(run,"lastHit"):
		start = 0
	else:
		start = run.lastHit
		for line in text[start:].split("\n"):
#			sys.stderr.write(line+"\n")
			for reg,fields in rules:
				hit = re.search(reg,line)
				if hit is not None:
					sys.stderr.write(str(hit.group())+"\n")
					for g,n in fields:
						run.log_scalar(n,float(hit.group(g)))
	return text,0

#ex.captured_out_filter = parseStdOut
#server = "localhost:27017"
#if "SACRED_DATABASE" in os.environ:
#	server = os.environ["SACRED_DATABASE"]
#ex.observers.append(MongoObserver.create(url=server))
#ex.observers.append(FileStorageObserver.create(os.environ["HOME"]+'/logs'))
from sacred.settings import SETTINGS
SETTINGS.CAPTURE_MODE = 'fd'


@ex.config
def myConfig():
	numberOfEpoches = 40
	kernel = "arccosine"
	loss = "squaredhinge"
	sampleSize = 5000
	nystroem = True
	conditioning = True
	exactConditioning = True
	loader = fact_sample.load
	K=5000
from random import shuffle
@ex.automain
def main(_run,numberOfEpoches,kernel,loss,sampleSize,nystroem,conditioning,loader,K,exactConditioning):
	configs = []
	for s in range(0,20,2):
		for e in range(80,101,2):
			for t in range(0,10,2):
				configs.append((s,e,t))
	shuffle(configs)
	configs[0]=(10,88,2)
	for s,e,t in configs:
				m = PyKernelMachine()
				print("lade die daten für ",s," ",e," ",t)
				num_classes,x_train,x_test,y_train,y_test,n,D,R,d = loader(s,e,t,integer_labels=True)
				print("R",R)
				m.fit(  x_train.astype(np.double),
					y_train.astype(np.int32),
					#val_x=x_test.astype(np.double),
					#val_y=y_test.astype(np.int32),
					numberOfEpoches=numberOfEpoches,
					kernel=kernel,
					loss=loss,
					K=K,
					sampleSize=sampleSize,
					nystroem=nystroem,
					conditioning=conditioning,
					exactConditioning=exactConditioning)
				statistics = m.predictStatistics(x_test.astype(np.double))
				print(statistics.shape)
				print(statistics[0,0]," ",statistics[0,1])
				print("ROC",roc_auc_score(y_test==1,statistics[:,1]))
				print(classification_report(y_test,np.argmax(statistics,axis=1)))
				print(confusion_matrix(y_test,np.argmax(statistics,axis=1)))
				print("Das war das Ergebnis für ",s," ",e," ",t)
