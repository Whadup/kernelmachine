import numpy as np
from kernelmachine import PyKernelMachine
import imdb
from sacred import Experiment
#from sacred.observers import MongoObserver
#ex.observers.append(MongoObserver.create())
from sacred.settings import SETTINGS
from sacred.observers import MongoObserver,FileStorageObserver
import os,sys
SETTINGS.CAPTURE_MODE = 'fd'

def parseStdOut(text,run):
	rules = [
		("Accuracy: (?P<acc>[0-9.]+) Loss: (?P<loss>[0-9.]+)",[("acc","Accuracy"),("loss","Loss")]),
		("Epoche duration (?P<secs>[0-9]+) seconds",[("secs","Epoche Time")]),
		("Setup Time (?P<secs>[0-9]+)$",[("secs","Setup Time")]),
		("Validation Accuracy (?P<acc>[0-9]+)$",[("acc","Validation Accuracy")])

	]
	import re
	if not hasattr(run,"lastHit"):
		start = 0
	else:
		start = run.lastHit
		for line in text[start:].split("\n"):
			sys.stderr.write(line+"\n")
			for reg,fields in rules:
				hit = re.search(reg,line)
				if hit is not None:
					sys.stderr.write(str(hit.group())+"\n")
					for g,n in fields:
						run.log_scalar(n,float(hit.group(g)))
	return text,0


ex = Experiment("Full IMDB")
# ex.captured_out_filter = parseStdOut


# server = "localhost:27017"
# if "SACRED_DATABASE" in os.environ:
# 	server = os.environ["SACRED_DATABASE"]
# ex.observers.append(MongoObserver.create(url=server))
# ex.observers.append(FileStorageObserver.create(os.environ["HOME"]+'/logs'))
from sacred.settings import SETTINGS
SETTINGS.CAPTURE_MODE = 'fd'	

@ex.config
def myConfig():
	numberOfEpoches = 1
	kernel = "arccosine"
	loss = "hinge"
	sampleSize = 2000
	nystroem = True
	conditioning = True
	K = 160

@ex.main
def main(_run,numberOfEpoches,kernel,loss,sampleSize,nystroem,conditioning,K):
	print("test")
	m = PyKernelMachine()
	print(kernel)
	m.fit(  x_train.astype(np.double)[:4000],
			y_train.astype(np.int32)[:4000],
			val_x=x_test.astype(np.double)[:1000],
			val_y=y_test.astype(np.int32)[:1000],
			numberOfEpoches=numberOfEpoches,
			kernel=kernel,
			loss=loss,
			K=K,
			sampleSize=sampleSize,
			nystroem=nystroem,
			conditioning=conditioning)
	del m
	print("done")

if __name__ == '__main__':
	#num_classes,x_train,x_test,y_train,y_test,n,D,R,d = imdb.load(integer_labels=True)
	for k in ["arccosine","invertedpolynomial","rbf"]:
		for l in ["squaredhinge","rmse","hinge"]:
			for c in [True,False]:
				for n in [True]:
					if not c:
						update = {"kernel":k,"loss":l,"conditioning":c,"nystroem":n}
						if n:
							update["sampleSize"]=10000
						call = "python3 ${EXPERIMENT} with "
						for x in update:
							call+=x+"="+str(update[x])+" "
						# ex.name="MNIST-"+k+"-"+l+"-"+str(c)+"-"+str(n)
						print(call)
					else:
						for K in [100,1000,10000]:
							update = {"kernel":k,"loss":l,"conditioning":c,"nystroem":n}
							if n:
								update["sampleSize"]=10000
							update["K"]=K
							call = "python3 ${EXPERIMENT} with "
							for x in update:
								call+=x+"="+str(update[x])+" "
							# ex.name="MNIST-"+k+"-"+l+"-"+str(c)+"-"+str(n)
							print(call)
						# ex.run(config_updates=update)
						# print("done2")

