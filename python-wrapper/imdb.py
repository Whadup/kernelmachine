import keras
import numpy as np
from keras.datasets import imdb
from scipy.sparse import csr_matrix
def load(integer_labels=True):
	(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=10000,
													  skip_top=30,
													  maxlen=None
													  )
	indptr = [0]
	indices = []
	data = []
	vocabulary = {}
	for d in x_train:
		for index in d:
			indices.append(index)
			data.append(1.0/len(d))
		indptr.append(len(indices))
	x_train = csr_matrix((data, indices, indptr), dtype=float).toarray()

	indptr = [0]
	indices = []
	data = []
	vocabulary = {}
	for d in x_test:
		for index in d:
			indices.append(index)
			data.append(1.0/len(d))
		indptr.append(len(indices))
	x_test = csr_matrix((data, indices, indptr), dtype=float).toarray()
	print("Load IMDB dataset.")
	print(x_train.shape[0], 'train samples')
	print(x_test.shape[0], 'test samples')
	print(x_train.shape[1],"features")
	#x_train = keras.preprocessing.text.one_hot(x_train,10000)
	print(x_train[0])
	#x_test = keras.preprocessing.text.one_hot(x_test,10000)
	n, D = x_train.shape    # (n_sample, n_feature)
	d = np.int32(n / 2) * 2 # number of random features
	num_classes = 2
	# convert class vectors to binary class matrices
	if not integer_labels:
		y_train = keras.utils.to_categorical(y_train, num_classes)
		y_test = keras.utils.to_categorical(y_test, num_classes)
	R = np.max(np.linalg.norm(x_train,2,axis=1))**2
	# x_train/=R
	# x_test/=R
	# R = 1.0
	return num_classes,x_train,x_test,y_train,y_test,n,D,R,d