# Python running example
from sacred import Experiment
# from __future__ import print_function

import numpy as np
from kernelmachine import PyKernelMachine
import mnist,cifar,susy,imdb
# Initialising the wrapped c++ function
import sys,os
from sacred.observers import MongoObserver,FileStorageObserver
ex = Experiment()

def parseStdOut(text,run):
	rules = [
		("Accuracy: (?P<acc>[0-9.]+) Loss: (?P<loss>[0-9.]+)",[("acc","Accuracy"),("loss","Loss")]),
		("Epoche duration (?P<secs>[0-9]+) seconds",[("secs","Epoche Time")]),
		("Setup Time (?P<secs>[0-9]+)$",[("secs","Setup Time")]),
		("Validation Accuracy (?P<acc>[0-9.]+)$",[("acc","Validation Accuracy")])

	]
	import re
	if not hasattr(run,"lastHit"):
		start = 0
	else:
		start = run.lastHit
		for line in text[start:].split("\n"):
			sys.stderr.write(line+"\n")
			for reg,fields in rules:
				hit = re.search(reg,line)
				if hit is not None:
					sys.stderr.write(str(hit.group())+"\n")
					for g,n in fields:
						run.log_scalar(n,float(hit.group(g)))
	return text,0

ex.captured_out_filter = parseStdOut
server = "localhost:27017"
if "SACRED_DATABASE" in os.environ:
	server = os.environ["SACRED_DATABASE"]
ex.observers.append(MongoObserver.create(url=server))
ex.observers.append(FileStorageObserver.create(os.environ["HOME"]+'/logs'))
from sacred.settings import SETTINGS
SETTINGS.CAPTURE_MODE = 'fd'


@ex.config
def myConfig():
	numberOfEpoches = 10
	kernel = "arccosine"
	loss = "hinge"
	sampleSize = 8192
	nystroem = True
	conditioning = True
	loader = imdb.load
	K = 160

@ex.automain
def main(_run,numberOfEpoches,kernel,loss,sampleSize,nystroem,conditioning,loader,K):

	m = PyKernelMachine()

	# num_classes,x_train,x_test,y_train,y_test,n,D,R,d = mnist.load(integer_labels=True)
	num_classes,x_train,x_test,y_train,y_test,n,D,R,d = loader(integer_labels=True)
	# m.fit(x_train.astype(np.double),y_train.astype(np.int32))

	#nystroemsample = x_train #np.random.permutation(x_train.shape[0])[:4096]
	print(kernel)
	m.fit(  x_train.astype(np.double),
			y_train.astype(np.int32),
			val_x=x_test.astype(np.double),
			val_y=y_test.astype(np.int32),
			numberOfEpoches=numberOfEpoches,
			kernel=kernel,
			loss=loss,
			K=K,
			sampleSize=sampleSize,
			nystroem=nystroem,
			conditioning=conditioning)