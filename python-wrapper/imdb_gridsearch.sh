#!/bin/bash
EXPERIMENT="imdb_experiment.py"
export SACRED_DATABASE=pfahler-db
export LD_LIBRARY_PATH="/home/pfahler/kernelmachine/python-wrapper"
export MKL_DYNAMIC=FALSE
cd $LD_LIBRARY_PATH
python3 ${EXPERIMENT} with kernel=arccosine loss=squaredhinge conditioning=True nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=arccosine loss=squaredhinge conditioning=True nystroem=False 
python3 ${EXPERIMENT} with kernel=arccosine loss=squaredhinge conditioning=False nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=arccosine loss=squaredhinge conditioning=False nystroem=False 
python3 ${EXPERIMENT} with kernel=arccosine loss=rmse conditioning=True nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=arccosine loss=rmse conditioning=True nystroem=False 
python3 ${EXPERIMENT} with kernel=arccosine loss=rmse conditioning=False nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=arccosine loss=rmse conditioning=False nystroem=False 
python3 ${EXPERIMENT} with kernel=arccosine loss=hinge conditioning=True nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=arccosine loss=hinge conditioning=True nystroem=False 
python3 ${EXPERIMENT} with kernel=arccosine loss=hinge conditioning=False nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=arccosine loss=hinge conditioning=False nystroem=False 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=squaredhinge conditioning=True nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=squaredhinge conditioning=True nystroem=False 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=squaredhinge conditioning=False nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=squaredhinge conditioning=False nystroem=False 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=rmse conditioning=True nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=rmse conditioning=True nystroem=False 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=rmse conditioning=False nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=rmse conditioning=False nystroem=False 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=hinge conditioning=True nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=hinge conditioning=True nystroem=False 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=hinge conditioning=False nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=invertedpolynomial loss=hinge conditioning=False nystroem=False 
python3 ${EXPERIMENT} with kernel=rbf loss=squaredhinge conditioning=True nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=rbf loss=squaredhinge conditioning=True nystroem=False 
python3 ${EXPERIMENT} with kernel=rbf loss=squaredhinge conditioning=False nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=rbf loss=squaredhinge conditioning=False nystroem=False 
python3 ${EXPERIMENT} with kernel=rbf loss=rmse conditioning=True nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=rbf loss=rmse conditioning=True nystroem=False 
python3 ${EXPERIMENT} with kernel=rbf loss=rmse conditioning=False nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=rbf loss=rmse conditioning=False nystroem=False 
python3 ${EXPERIMENT} with kernel=rbf loss=hinge conditioning=True nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=rbf loss=hinge conditioning=True nystroem=False 
python3 ${EXPERIMENT} with kernel=rbf loss=hinge conditioning=False nystroem=True sampleSize=10000 
python3 ${EXPERIMENT} with kernel=rbf loss=hinge conditioning=False nystroem=False 

