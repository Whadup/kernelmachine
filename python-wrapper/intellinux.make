#GFORTRANPATH = /usr/local/gfortran/lib/
#PYTHONPATH = /Users/pfahler/anaconda3/include/python3.6m
#NUMPYPATH = /Users/pfahler/anaconda3/lib/python3.6/site-packages/numpy/core/include
#CYSIGNALSPATH = /Users/pfahler/anaconda3/lib/python3.6/site-packages/cysignals/

PYTHONPATH = /usr/include/python3.5m
NUMPYPATH = /usr/lib/python3/dist-packages/numpy/core/include/# /Users/pfahler/anaconda3/lib/python3.6/site-packages/numpy/core/include
CYSIGNALSPATH = /usr/local/lib/python3.5/dist-packages/cysignals/

COMPILER_FLAGS = -fPIC -O3 -g3 -mtune=native -march=native -mkl -DUSE_INTEL_MKL
# COMPILER_FLAGS = -O0 -g3
BUNDLE_FLAGS =  -mkl -qopenmp -g3 -O3 -shared -march=native -DUSE_INTEL_MKL 
#-lmkl_rt -lmkl_def
# BUNDLE_FLAGS =  -Xpreprocessor -qopenmp -g3 -O3 -bundle -march=native 


all:
	cythonize kernelmachine.pyx 
	icpc -I${NUMPYPATH} -I ${PYTHONPATH} -I ${CYSIGNALSPATH} ${COMPILER_FLAGS} -c kernelmachine.cpp
	icpc ../*.o kernelmachine.o ${BUNDLE_FLAGS} -static-intel -o kernelmachine.so
	#check if python can import
	python3 -c "from kernelmachine import PyKernelMachine;print(PyKernelMachine)"

debug: CXXFLAGS += -g -O0 -fno-inline
debug: all