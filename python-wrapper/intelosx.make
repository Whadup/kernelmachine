GFORTRANPATH = /usr/local/gfortran/lib/
PYTHONPATH = /Users/pfahler/anaconda3/include/python3.6m
NUMPYPATH = /Users/pfahler/anaconda3/lib/python3.6/site-packages/numpy/core/include
CYSIGNALSPATH = /Users/pfahler/anaconda3/lib/python3.6/site-packages/cysignals/
COMPILER_FLAGS = -O3 -g3 -std=c++11 -mtune=native -march=native -mkl
BUNDLE_FLAGS =  -mkl -qopenmp -g3 -O3 -bundle -undefined dynamic_lookup -march=native 

all:
	cythonize kernelmachine.pyx 
	icpc -I${NUMPYPATH} -I ${PYTHONPATH} -I ${CYSIGNALSPATH} ${COMPILER_FLAGS} -c kernelmachine.cpp
	icpc ${BUNDLE_FLAGS} ../*.o kernelmachine.o -o kernelmachine.cpython-36m-darwin.so	
	#check if python can import
	python3 -c "from kernelmachine import PyKernelMachine;print(PyKernelMachine)"

debug: CXXFLAGS += -g3 -O0 -fno-inline
debug: all