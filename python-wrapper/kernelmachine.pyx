# distutils: language = c++
# distutils: sources = "../kernelmachine.cpp"

# Cython interface file for wrapping the object
#
#

from libcpp.vector cimport vector
import numpy as np
cimport numpy as np
from cysignals.signals cimport sig_on, sig_off
import cython
# c++ interface to cython
cdef extern from "../kernelmachine.h":
  cdef cppclass KernelMachine:
        int numberOfLabels 
        KernelMachine() except +
        void fit(double* X, int* Y, int N, int d, double* valX, int* valY, int valN,
    int numberOfEpoches, int K, int sampleSize,int kernelId, int lossId, int nystroem, int conditioning, int exactConditioning) except +
        void nystroem(double* x, int* y,int N, int d) except +
        void predict(double* x, int* y,int N, int d)
        void predictStatistics(double* x, double* y,int N, int d)

# creating a cython wrapper class
cdef class PyKernelMachine:
    cdef KernelMachine *thisptr      # hold a C++ instance which we're wrapping
    def __cinit__(self):
        self.thisptr = new KernelMachine()
        print("kernel machine allocated")
    def __dealloc__(self):
        del self.thisptr
    # def fit(self, sx,sy):
    #     return self.thisptr.fit(sx,sy)
    @cython.boundscheck(False)
    @cython.wraparound(False)
    def fit(self, np.ndarray[double, ndim=2, mode="c"] x not None,
            np.ndarray[int, ndim=1, mode="c"] y not None,
            np.ndarray[double, ndim=2, mode="c"] val_x=None,
            np.ndarray[int, ndim=1, mode="c"] val_y=None,
            numberOfEpoches=10,K=160,sampleSize=2400,kernel="rbf",loss="hinge",nystroem=False, conditioning=True, exactConditioning=True):
        cdef int kernelId
        cdef int lossId = 0
        if kernel == "rbf":
            kernelId = 0
        elif kernel == "chisquared":
            kernelId = 1
        elif kernel == "invertedpolynomial":
            kernelId = 2
        elif kernel == "arccosine":
            kernelId = 3
        if loss == "hinge":
            lossId = 1
        elif loss == "rmse":
            lossId = 0  
        elif loss == "squaredhinge":
            lossId = 2  
        print(kernelId)
        cdef int m, n
        cdef int val_n
        m, n = x.shape[0], x.shape[1]
        sig_on()
        if val_x is None:
            self.thisptr.fit(&x[0,0], &y[0], m, n, NULL, NULL, 0, numberOfEpoches, K, sampleSize, kernelId, lossId, nystroem, conditioning,exactConditioning)
        else:           
            val_n = val_x.shape[0]
            self.thisptr.fit(&x[0,0], &y[0], m, n, &val_x[0,0], &val_y[0], val_n,numberOfEpoches, K, sampleSize,kernelId, lossId, nystroem, conditioning,exactConditioning)
        sig_off()
        # return None
    def predict(self, np.ndarray[double, ndim=2, mode="c"] x not None):
        #we need to return a numpy array here. but that's something for the future
        cdef int m,n 
        m, n = x.shape[0], x.shape[1]
        cdef np.ndarray[int, ndim=1, mode="c"] y = np.zeros(m,dtype=np.int32)
        sig_on()
        self.thisptr.predict(&x[0,0], &y[0], m, n)
        sig_off()
        return 
    def predictStatistics(self, np.ndarray[double, ndim=2, mode="c"] x not None):
        #we need to return a numpy array here. but that's something for the future
        cdef int m,n 
        m, n = x.shape[0], x.shape[1]
        cdef np.ndarray[double, ndim=2, mode="fortran"] y = np.zeros((m,self.thisptr.numberOfLabels),dtype=np.double,order="F")
        sig_on()
        self.thisptr.predictStatistics(&x[0,0], &y[0,0], m, n)
        sig_off()
        return y
