GFORTRANPATH = /usr/local/gfortran/lib/
PYTHONPATH = /usr/include/python3.5m
NUMPYPATH = /usr/local/lib/python3.5/dist-packages/numpy/core/include/# /Users/pfahler/anaconda3/lib/python3.6/site-packages/numpy/core/include
COMPILER_FLAGS = -O3 -g3 -mtune=native -march=native
CYSIGNALSPATH = /usr/local/lib/python3.5/dist-packages/cysignals/

all:
	cython3 --cplus kernelmachine.pyx
	clang++ -fPIC -I${NUMPYPATH} -I ${PYTHONPATH} -I ${CYSIGNALSPATH} ${COMPILER_FLAGS} -c kernelmachine.cpp
	clang++ -fopenmp -g3 -lpython3.5m -shared -fPIC -fwrapv -march=native  ../*.o ../propack/*.o ../propack/Lapack_Util/*.o kernelmachine.o -L${GFORTRANPATH} -lgfortran -lblas -o kernelmachine.so	
	#check if python can import
	python3 -c "from kernelmachine import PyKernelMachine;print(PyKernelMachine)"

#	cythonize kernelmachine.pyx 
#	g++ -I${NUMPYPATH} -I ${PYTHONPATH} -I ${CYSIGNALSPATH} ${COMPILER_FLAGS} -c kernelmachine.cpp
#	g++ -Xpreprocessor -fopenmp -lomp -g3 -bundle -undefined dynamic_lookup -march=native  ../*.o ../propack/*.o ../propack/Lapack_Util/*.o kernelmachine.o -L${GFORTRANPATH} -lgfortran -lblas -o kernelmachine.cpython-36m-darwin.so	
#	#check if python can import
#	python3 -c "from kernelmachine import PyKernelMachine;print(PyKernelMachine)"