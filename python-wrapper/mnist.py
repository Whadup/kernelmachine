import numpy as np
import keras
from keras.datasets.mnist import load_data as mnist_data


def unit_range_normalize(X):
	min_ = np.min(X, axis=0)
	max_ = np.max(X, axis=0)
	diff_ = max_ - min_
	diff_[diff_<=0.0] = np.maximum(1.0, min_[diff_<=0.0])
	SX = (X - min_) / diff_
	return SX

def prod(x):
	s = 1
	for xx in x:
		s*=xx
	return s

def load(convolution=False,integer_labels=False):
	# input image dimensions
	img_rows, img_cols = 28, 28
	
	# the data, shuffled and split between train and test sets
	(x_train, y_train), (x_test, y_test) = mnist_data()
	#if not convolution:
	x_train = x_train.reshape(x_train.shape[0], img_rows * img_cols)
	x_test = x_test.reshape(x_test.shape[0], img_rows * img_cols)
	
	x_train = x_train.astype('float32') / 255
	x_test = x_test.astype('float32') / 255
	
	x_train = unit_range_normalize(x_train)
	x_test = unit_range_normalize(x_test)
	print("Load MNIST dataset.")
	print(x_train.shape[0], 'train samples')
	print(x_test.shape[0], 'test samples')
	#print(x_train[0])
	# convert class vectors to binary class matrices
	num_classes = 10
	if not integer_labels:
		y_train = keras.utils.to_categorical(y_train, num_classes)
		y_test = keras.utils.to_categorical(y_test, num_classes)
	R = np.max(np.linalg.norm(x_train,2,axis=1))**2
	
	n = x_train.shape[0]    # (n_sample, n_feature)
	D = prod(x_train.shape[1:])
	d = np.int32(n / 2) * 2 # number of random features
	x_train = keras.utils.normalize(x_train,axis=1)
	x_test = keras.utils.normalize(x_test,axis=1)
	#print(x_train[0].shape)
	return num_classes,x_train,x_test,y_train,y_test,n,D,R,d
