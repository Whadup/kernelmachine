# Python running example
from sacred import Experiment
# from __future__ import print_function

import numpy as np
from kernelmachine import PyKernelMachine
import nico
# Initialising the wrapped c++ function
import sys,os
from sacred.observers import MongoObserver,FileStorageObserver
from sklearn.model_selection import StratifiedKFold,KFold
ex = Experiment()

def parseStdOut(text,run):
	rules = [
		("Accuracy: (?P<acc>[0-9.]+) Loss: (?P<loss>[0-9.]+)",[("acc","Accuracy"),("loss","Loss")]),
		("Epoche duration (?P<secs>[0-9]+) seconds",[("secs","Epoche Time")]),
		("Setup Time (?P<secs>[0-9]+)$",[("secs","Setup Time")]),
		("Validation Accuracy (?P<acc>[0-9.]+)$",[("acc","Validation Accuracy")])

	]
	import re
	if not hasattr(run,"lastHit"):
		start = 0
	else:
		start = run.lastHit
		for line in text[start:].split("\n"):
#			sys.stderr.write(line+"\n")
			for reg,fields in rules:
				hit = re.search(reg,line)
				if hit is not None:
					sys.stderr.write(str(hit.group())+"\n")
					for g,n in fields:
						run.log_scalar(n,float(hit.group(g)))
	return text,0

ex.captured_out_filter = parseStdOut
server = "localhost:27017"
if "SACRED_DATABASE" in os.environ:
	server = os.environ["SACRED_DATABASE"]
# ex.observers.append(MongoObserver.create(url=server))
# ex.observers.append(FileStorageObserver.create(os.environ["HOME"]+'/logs'))
from sacred.settings import SETTINGS
SETTINGS.CAPTURE_MODE = 'fd'


@ex.config
def myConfig():
	numberOfEpoches = 200
	kernel = "arccosine"
	loss = "squaredhinge"
	sampleSize = 2300
	nystroem = True
	conditioning = True
	loader = nico.load
	K = 5000

@ex.automain
def main(_run,numberOfEpoches,kernel,loss,sampleSize,nystroem,conditioning,loader,K):
	# num_classes,x_train,x_test,y_train,y_test,n,D,R,d = mnist.load(integer_labels=True)
	num_classes,X,y,y_train,y_test,n,D,R,d = loader(integer_labels=True)
	# m.fit(x_train.astype(np.double),y_train.astype(np.int32))
	
	confusion = [num_classes*[0] for i in range(num_classes)]

	skf = KFold(n_splits=10,shuffle=True)
	totalAcc = 0
	for train_index, test_index in skf.split(X, y):
		m = PyKernelMachine()
		x_train, x_test = X[train_index], X[test_index]
		y_train, y_test = y[train_index], y[test_index]
		m.fit(  x_train.astype(np.double),
			y_train.astype(np.int32),
			#val_x=x_test.astype(np.double),
			#val_y=y_test.astype(np.int32),
			numberOfEpoches=numberOfEpoches,
			kernel=kernel,
			loss=loss,
			K=K,
			sampleSize=sampleSize,
			nystroem=nystroem,
			conditioning=conditioning,
			exactConditioning=True)
		pred = m.predict(x_test.astype(np.double))
		acc = 0.0
		for i in range(len(y_test)):
			confusion[int(y_test[i])][int(pred[i])]+=1
			if y_test[i]==pred[i]:
				acc+=1.0/len(y_test)
		print(acc)
		totalAcc+=acc/10.0

	print("Cross Validation Accuracy ",totalAcc)
	for i in confusion:
		print(i)



