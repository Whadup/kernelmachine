GFORTRANPATH = /usr/local/gfortran/lib/
PYTHONPATH = /Users/pfahler/anaconda3/include/python3.6m
NUMPYPATH = /Users/pfahler/anaconda3/lib/python3.6/site-packages/numpy/core/include
CYSIGNALSPATH = /Users/pfahler/anaconda3/lib/python3.6/site-packages/cysignals/
COMPILER_FLAGS = -O3 -g3 -mtune=native -march=native
# COMPILER_FLAGS = -O0 -g3
BUNDLE_FLAGS =  -Xpreprocessor -fopenmp -lomp -g3 -O3 -bundle -undefined dynamic_lookup -march=native 


all:
	cythonize kernelmachine.pyx 
	g++ -I${NUMPYPATH} -I ${PYTHONPATH} -I ${CYSIGNALSPATH} ${COMPILER_FLAGS} -c kernelmachine.cpp
	# g++ -Xpreprocessor -bundle -undefined dynamic_lookup -march=native  ../*.o ../propack/*.o ../propack/Lapack_Util/*.o kernelmachine.o -L${GFORTRANPATH} -lgfortran -lblas -o kernelmachine.cpython-36m-darwin.so	
	g++ ${BUNDLE_FLAGS} ../*.o ../propack/*.o ../propack/Lapack_Util/*.o kernelmachine.o -L${GFORTRANPATH} -lgfortran -lblas -o kernelmachine.cpython-36m-darwin.so	
	#check if python can import
	python3 -c "from kernelmachine import PyKernelMachine;print(PyKernelMachine)"
