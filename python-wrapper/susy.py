

from sklearn.model_selection import train_test_split
import keras
import numpy as np

def scale_linear_bycolumn(X, high=1.0, low=0.0):
    mins = np.min(X, axis=0)
    maxs = np.max(X, axis=0)+0.00000001
    rng = maxs - mins
    return high - (((high - low) * (maxs - X)) / rng)

def load(greyscale=True,integer_labels=False):
	num_classes = 2	    # number of classes
	data = np.genfromtxt("/data/d1/SUSY.csv", delimiter=',')
	X = data[:,1:]
	Y = data[:,0]

	x_train,x_test,y_train,y_test = train_test_split(scale_linear_bycolumn(X), Y, test_size=0.2)
	
	print('x_train shape:', x_train.shape)
	# print(x_train[0],x_train.dtype)
	n, D = x_train.shape    # (n_sample, n_feature)
	
	print("Load SUSY dataset.")
	print(x_train.shape[0], 'train samples')
	print(x_test.shape[0], 'test samples')

	d = np.int32(n / 2) * 2 # number of random features
	#x_train = keras.utils.normalize(x_train,axis=1)
	#x_test = keras.utils.normalize(x_test,axis=1)
	# convert class vectors to binary class matrices
	print(y_train.shape)
	if not integer_labels:
		y_train = keras.utils.to_categorical(y_train, num_classes)
		y_test = keras.utils.to_categorical(y_test, num_classes)
	else:
		y_train = y_train.reshape(y_train.shape[0])
		y_test = y_test.reshape(y_test.shape[0])
	R = np.max(np.linalg.norm(x_train,2,axis=1))**2
	return num_classes,x_train,x_test,y_train,y_test,n,D,R,d	

