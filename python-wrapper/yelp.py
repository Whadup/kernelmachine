from sklearn.model_selection import train_test_split
import keras
import numpy as np
import json
from sklearn.feature_extraction.text import CountVectorizer 

def load(integer_labels=False):
		num_classes = 5	    # number of classes

		print("Loading reviews")
		texts = []
		Y = []

		print("Loading businesses")
		businesses = {}
		f = open("/data/business.json",'r')
		for line in f:
				jsonEntry = json.loads(line.strip())
				businesses[jsonEntry['business_id']] = jsonEntry['categories']


		f = open("/data/review.json",'r')
		for line in f:
				jsonEntry = json.loads(line.strip())
				#langs = detect(jsonEntry['text'])
				if "Restaurants" in businesses[jsonEntry['business_id']]:
					texts.append(jsonEntry['text'])
					Y.append(jsonEntry['stars'])
				if len(Y)%100 == 0:
					print(len(Y))
		count_vect = CountVectorizer(stop_words='english', lowercase=True,max_df=0.1, max_features=10000)
		X = count_vect.fit_transform(texts[:1000000])

		print("Data shape:",X.shape)
		# x_train = X[:2000000]
		# y_train = Y[:2000000]
		# x_test = X[2000000:]
		# y_test = Y[2000000:]
		x_train,x_test,y_train,y_test = train_test_split(X, Y[:1000000], test_size=0.25)
		n, D = x_train.shape    # (n_sample, n_feature)
		d = np.int32(n / 2) * 2 # number of random features
		R = np.max(np.linalg.norm(x_train,2,axis=1))**2
		return num_classes,x_train,x_test,y_train,y_test,n,D,R,d