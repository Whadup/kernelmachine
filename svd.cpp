#include "svd.h"
#include <iostream>

#ifdef USE_INTEL_MKL
#include <mkl.h>
#endif
#ifndef USE_INTEL_MKL

int svd(int m, 
	int n, 
	int k,
	void (*aprod)(	const char *transa,
					int *m,
					int *n,
					double *x,
					double *y,
					double *dparm,
					int *iparm),
	double* UU,
	double* VU,
	double* pS,
	double anorm,
	void* user
	)
{
	char YES = 'y';
	int N=n;
	int M=m;
	int K=k;
	int KMAX = 3*k - 1;
	// std::cout << "KMAX" <<KMAX << std::endl;
	
	int LDU = M; 
	//#warning "ldu and ldv not set"
	double* U = new double[LDU*(KMAX+1)];
	for(int i=0;i<LDU*(KMAX+1);i++)
		U[i]=0;

	double* BND = new double[K];

	
	int LDV = N;
	double* V = new double[LDV*(KMAX)];
	for(int i=0;i<LDU*(KMAX);i++)
		V[i]=0;	
		

	char e = 'e';
	double eps = dlamch_(&e);
	
	double TOLIN = 16*eps;
	// std::cout << eps << " " << TOLIN << std::endl;
	//should be at least M + N + 9*KMAX + 5*KMAX**2 + 4 + MAX(3*KMAX**2+4*KMAX+4, NB*MAX(M,N))
	int LWORK = M + N + 10*KMAX + 6*KMAX*KMAX + 4 + max(3*KMAX*KMAX+4*KMAX+4, 2*max(M,N));
	double* WORK = new double[LWORK];
	int LIWORK = 10*KMAX;// Should be at least 8*KMAX if JOBU.EQ.'Y' or JOBV.EQ.'Y' and at least 2*KMAX+1 otherwise.
	int* IWORK = new int[LIWORK];

	double DOPTION[3];// = new double[3];
	DOPTION[0] = sqrt(eps/K);
	DOPTION[1] = 10*pow(eps,3.0/4.0);
	DOPTION[2] = anorm;//anorm?

	int IOPTION[2];// = new int[2];
	IOPTION[0] = 0;
	IOPTION[1] = 1;

	int INFO = -1; //okay

	// std::cout << "vor der svd " << K << " " << KMAX << std::endl;
	dlansvd_(&YES,&YES,&M,&N,&K,&KMAX,aprod,U,&LDU,pS,BND,
	V,&LDV,&TOLIN,WORK,&LWORK,IWORK,&LIWORK,DOPTION,IOPTION,
	&INFO,0,(int*)user);
	// std::cout << "nach der svd " << K << " " << KMAX << std::endl;
	
	for(int i=0;i<m;i++)
	{	for(int j=0;j<k;j++)
		{
			UU[j*m+i] = U[j*m+i];
		}
	}
	//do some memcopy to obtain U and V
	delete[] WORK;
	delete[] IWORK;
	delete[] BND;
	delete[] U;
	delete[] V;
	// std::cout << INFO << std::endl;
	if(INFO==0)
		return k;
	return INFO < k ? INFO : k;
}
#endif


#ifdef USE_INTEL_MKL
int eig(int m, int k, double* A, double* U, double* S,int lda)
{
	// double* a = new double[(m*m+m)/2];
	// a umlayouten
	//(int matrix_layout, char uplo, char trans, lapack_int n, double* ap, 
	//lapack_int i, lapack_int j, lapack_int rows, lapack_int cols, const double* a, lapack_int lda);
	// int returnValue = LAPACKE_mkl_dtppack(LAPACK_COL_MAJOR,'U','N',m,a,1,1,m,m,A,m);
	// std::cout << "Transformation failed with "<<returnValue << std::endl;
	// LAPACKE_dtrttp(LAPACK_COL_MAJOR , 'U' , m , A , m , a );
	// for(int i=0;i<m;i++)
	// {
	// 	for(int j=0;j<m;j++)
	// 	{
	// 		if(j<i)
	// 		{
	// 			std::cout << a[j + i*(i + 1)/2]<<"/"<<A[j*m+i]<< "\t";
	// 		}
	// 		else
	// 		{
	// 			std::cout << a[i + j*(j + 1)/2]<<"/"<<A[j*m+i]<< "\t";
	// 		}
	// 	}
	// 	std::cout << std::endl;
	// }
	/*lapack_int LAPACKE_dsyevr (
	int matrix_layout, 
	char jobz, 
	char range, 
	char uplo, 
	lapack_int n, 
	double* a, 
	lapack_int lda, 
	double vl, 
	double vu, 
	lapack_int il, 
	lapack_int iu, 
	double abstol, 
	lapack_int* m, 
	double* w, 
	double* z, 
	lapack_int ldz, 
	lapack_int* isuppz);*/
	mkl_verbose(1);
	double* UU = new double[m*k];
	double* SS = new double[m];
	// std::cout << "created temporary arrays for reordering later" << std::endl;
	int result = 0;
	int valuesComputed=0;
	if(k<m)
	{
	result = LAPACKE_dsyevr(
		LAPACK_COL_MAJOR,
		'V',
		'I',
		'U',
		m,
		A,
		lda,
		0.0,
		0.0,
		m-k+1,
		m,
		0.0,
		&valuesComputed,
		SS,
		UU,
		m,
		NULL
		);
	}
	else
	{
		lapack_int* l = new lapack_int[2*m];
		result = LAPACKE_dsyevr(
		LAPACK_COL_MAJOR,
		'V',
		'A',
		'U',
		m,
		A,
		lda,
		0.0,
		0.0,
		0,
		0,
		0.0,
		&valuesComputed,
		SS,
		UU,
		m,
		l
		);
		delete[] l;
	}
	// std::cout << "got through mkl" << std::endl;
	// for(int i=0;i<m;i++)
	// 	std::cout << SS[i]<< " ";
	// std::cout << std::endl;
	for(int i=0;i<k;i++)
	{	
		
		S[i]=SS[k-i-1];
		for(int j=0;j<m;j++)
			U[i*m+j] = UU[(k-1-i)*m+j];
		if(S[i]<0)
		{
			S[i]=0;
			valuesComputed = i;
			for(;i<k;i++)
				S[i]=0;
			break;
		}
		// std::cout<< SS[k-i-1] << " ";
		
	}
	// std::cout << " aufräumen" << std::endl;
	delete[] UU;
	delete[] SS;
	mkl_verbose(0);
	std::cout << valuesComputed << "Values computed " << result <<std::endl;
	return valuesComputed;
}
#endif
