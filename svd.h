#pragma once
#include <iostream>
#include <cmath>
#ifndef USE_INTEL_MKL
extern "C"
void dlansvd_(const char *jobu,
						 const char *jobv,
						 int *m,
						 int *n,
						 int *k,
						 int *kmax,
						 void (*aprod)(const char *transa,
													 int *m,
													 int *n,
													 double *x,
													 double *y,
													 double *dparm,
													 int *iparm),
						 double *U,
						 int *ldu,
						 double *Sigma,
						 double *bnd,
						 double *V,
						 int *ldv,
						 double *tolin,
						 double *work,
						 int *lwork,
						 int *iwork,
						 int *liwork,
						 double *doption,
						 int *ioption,
						 int *info,
						 double *dparm,
						 int *iparm);
extern "C"
double dlamch_(const char* e);
#endif

int svd(int m, 
	int n, 
	int k,
	void (*aprod)(	const char *transa,
					int *m,
					int *n,
					double *x,
					double *y,
					double *dparm,
					int *iparm),
	double* U,
	double* V,
	double* S,
	double Anorm,
	void* user
	);

int eig(int m, int k, double* A, double* U, double* S,int lda);

inline int max(int a, int b)
{
	return a>b ? a : b;
}
