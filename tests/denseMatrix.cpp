#include "matrix.h"
#include <iostream>
#include <random>
#include <cmath>
int main()
{
	int m = 20;
	int n = 15;
	int k = 10;
	DenseMatrix l;
	makeDenseMatrix(l,m,n);
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(-0.5, 0.5);
	for(int i=0;i<m;i++)
		for(int j=0;j<n;j++)
			l.X[j*m+i]=1+dis(gen);

	double* A = l.X;


	double* x = new double[n]{0};
	for(int i=0;i<n;i++)
		x[i]=dis(gen);
	double* y = new double[m]{0};

	denseMatrixVector(l,x,y,0,0);
	for(int i=0;i<m;i++)
		std::cout << y[i] << " ";
	std::cout << std::endl;
	for(int i=0;i<m;i++)
	{
		double s = 0;
		for(int j=0;j<n;j++)
			s+=A[j*m+i] * x[j];
		std::cout << s << " ";
	}
	std::cout << std::endl;

	for(int i=0;i<m;i++)
	 	y[i]=dis(gen);

	denseMatrixVector(l,y,x,1,0);
	for(int i=0;i<n;i++)
		std::cout << x[i] << " ";
	std::cout << std::endl;
	for(int i=0;i<n;i++)
	{
		double s = 0;
		for(int j=0;j<m;j++)
		{
			s+=A[i*m+j] * y[j];
		}
		std::cout << s << " ";
	}
	std::cout << std::endl;
}