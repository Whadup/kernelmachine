#include "util.h"

void randomSubset(int* indices,int* result,int N,int sampleSize)
{
	for (unsigned i = 0; i < sampleSize; i++)
	{
		unsigned const r = rand() % (N-i);
		result[i] = indices[r];
		indices[r] = indices[N-i-1];
		indices[N-i-1] = r;
	}
	for (unsigned i = N-sampleSize; i < N; i++)
	{
		// restore previously changed values
		indices[indices[i]] = indices[i];
		indices[i] = i;
	}
}
